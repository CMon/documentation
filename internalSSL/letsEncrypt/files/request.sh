#!/bin/sh

ACME_URL=${LETS_ENCRYPT_ACME_URL:-"https://acme-v02.api.letsencrypt.org/directory"}

cd /data
mkdir -p ./conf ./work ./logs

certbot --agree-tos --email ${ACCOUNT_INFO_EMAIL} --non-interactive \
    --config-dir /data/conf --work-dir /data/work --logs-dir /data/logs \
    --server ${ACME_URL} \
    --manual-auth-hook /scripts/createEntry.sh \
    --manual-cleanup-hook /scripts/cleanupEntry.sh \
    -d *.${CERT_DOMAIN} --manual --preferred-challenges dns-01 certonly

