= Sparse files

some filesystems do not support sparse files (ecryptfs) to have a check if your current filesystem supports sparse files run the script `./test-for-sparse-file-support.sh`.

Sparse files are used by bmap and similar to reduce image sizes
