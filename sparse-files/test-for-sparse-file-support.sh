#!/bin/bash

dd seek=1M if=/dev/zero of=sparse-test bs=1 count=1 status=none
if [ "$(du -s sparse-test | awk '{print $1}')" -lt "1000" ]
then
    echo "sparse supported."
else
    echo "sparse not supported."
fi
rm sparse-test
