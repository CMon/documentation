#!/bin/bash

IMAGE_FILE=${1}
SWITCH_DEVICE=${2:-"rb_power"}
SG_DEV=${3:-"/dev/sg1"}

SWITCH_DUT_OFF_CMD="curl --request POST http://192.168.2.238:33883/v1/output/${SWITCH_DEVICE}/off"
SWITCH_DUT_ON_CMD="curl --request POST http://192.168.2.238:33883/v1/output/${SWITCH_DEVICE}/on"


function getBmap {
    local -r imageFile=${1}
    local -r baseImageName=${imageFile%.*}
    if [ -f "${baseImageName}.bmap" ]      # bla-blub.img.bz2 and bla-blub.img.bmap
    then
        echo "${baseImageName}.bmap"
    elif [ -f "${imageFile}.bmap" ]        # bla-blub.img.bz2 and bla-blub.img.bz2.bmap
    then
        echo "${imageFile}.bmap"
    elif [ -f "${baseImageName%.*}.bmap" ] # bla-blub.img.bz2 and bla-blub.bmap
    then
        echo "${baseImageName%.*}.bmap"
    fi
    echo ""
}

function isSgModuleLoaded {
    modprobe sg
    if $(lsmod | grep sg > /dev/null 2>&1)
    then
        return 0
    fi
    return 1
}

function getLastInserted {
    local -r lastInsertedInBraces=$(dmesg | grep "logical blocks" | tail -n 1 | awk '{print $4}')
    echo "/dev/${lastInsertedInBraces:1:-1}"
}

function isSDMuxInstalled {
    export PATH="/$HOME/.local/bin":"$PATH"
    if ! which usbsdmux > /dev/null 2>&1
    then
        return 1
    fi
    return 0
}

if  ! `isSgModuleLoaded` 
then
    echo "You need to load the »sg« module with: modprobe sg"
    exit 1
fi

if [ ! -w "$SG_DEV" ]
then
    echo "$SG_DEV does not exist, or this use has no write permission on it"
    exit 2
fi

if ! isSDMuxInstalled
then
    echo "usbsdmux is not installed, install it with: »pip install --user usbsdmux«"
    exit 3
fi

bmapFile=`getBmap "${IMAGE_FILE}"`

# Switch off DUT
${SWITCH_DUT_OFF_CMD}

# Switch Card to Host mode
usbsdmux $SG_DEV host
sleep 5 # wait for sd? to be mounted

# flash card with bmap file
dest=`getLastInserted`
if [ "$bmapFile" = "" ]
then
    `dd if=${IMAGE_FILE} of=${dest} bs=10M status=progress`
else
    `bmaptool copy --bmap ${bmapFile} ${IMAGE_FILE} ${dest}`
fi

# Switch Card to dut mode
usbsdmux $SG_DEV dut
sleep 5 # wait for sd? to be available on the other side

# swtich on DUT
${SWITCH_DUT_ON_CMD}

