= enable debug for systemd services

as example the `systemd-resolved`

* run: `systemctl edit systemd-resolved`
* insert:
```
[Service]
Environment=SYSTEMD_LOG_LEVEL=debug
```
* restart service: `systemctl restart systemd-resolved`

CAUTION: Do not forget to revert the entry!!
