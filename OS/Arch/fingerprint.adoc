= use the fingerprint to unlock the kde screen saver

install:
. `fprintd`
. or `open-fprintd`, `python-validity` (this is necessary for fprintd unsupported sensors like: 'Synaptics, Inc. Metallica MIS Touch Fingerprint Reader')

and add
----
auth            sufficient      pam_unix.so try_first_pass likeauth nullok
auth            sufficient      pam_fprintd.so
----

to the top of /etc/pam.d/kde (above all the other entries)

now on the lock screen hit twice enter then you are prompted to use the fingerprint
