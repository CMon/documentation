= Skip Validity check

Some Packages update very often and do not provide a seperate url for each release (for example: simplicityStudio from SiliconLabs). Then the sum is always wrong for the aur package. If you still want to update/install this package
run something similar to:

----
$ yaourt -Sy simplicitystudio --m-arg "--skipchecksums"
----

Best is to install all deps without this just to be sure and then when the simplicity studio fails run the above command.

.Example error message
----
==> ERROR: One or more files did not pass the validity check!
==> ERROR: Makepkg was unable to build .
----
