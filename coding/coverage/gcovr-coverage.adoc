= Using `gcovr` as coverage collector
This tool supports clang(llvm-cov) and gcc builds

`gcovr` seems to be the successor of `lcov`, it supports text output similar to pythons `coverage` tool and xml outputs to Cobertura and Sonarqube formats, and the well known genhtml html output.

.Compile the code
. Add the following flags to your CFLAGS/CXXFLAGS 
----
-fprofile-arcs -ftest-coverage -fPIC
----

INFO| For using llvm/clang builds with gcovr you need to set the argument `--gcov-executable "llvm-cov gcov"` additional to the others

.Get HTML output
----
$ mkdir -p /tmp/htmlOutput/
$ gcovr -r . --html -o /tmp/htmlOutput/index.html --html-details
----

.If you want to split up into single tests but then also have merged coverage you can merge multiple json tracefiles:
. For each run, generate JSON output:
+
----
...  # compile and run first test case
$ gcovr ... --json run-1.json
...  # compile and run second test case
$ gcovr ... --json run-2.json
----
. Next, merge the json files and generate the desired report:
+
----
gcovr --add-tracefile run-1.json --add-tracefile run-2.json --html-details coverage.html
----
