= Ignoring files or specific code points for static code analysis

.Ignoring includes:
* add them as system libraries: `include_directories(SYSTEM ${CMAKE_BINARY_DIR}/include)` 

.Ignoring source code files:
* add them as static library and disable clang-tidy on them:
+
----
set(THIRD_PARTY_SRC
...
)

set(THIRD_PARTY_HDR
...
)
add_library(${LIB_NAME}_to_ignore STATIC ${THIRD_PARTY_HDR} ${THIRD_PARTY_SRC})
set_target_properties(${LIB_NAME}_to_ignore PROPERTIES CXX_CLANG_TIDY "")
----

.Ignoring lines of code:
* all errors on next line: `// NOLINTNEXTLINE`
* specific error on next line: `// NOLINTNEXTLINE(clang-analyzer-cplusplus.NewDeleteLeaks)`
* all errors on current line: `// NOLINT`
* specific error on current line: `// NOLINT(clang-diagnostic-unused-macros)`
