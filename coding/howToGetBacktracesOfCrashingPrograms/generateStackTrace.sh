#!/bin/bash

TRACEFILE=""
OVERWRITE=false
LIBSEGFAULT=""

function usage()
{
    echo $0" [-h] [-t <trace-file> [-o]] -- <program> [<program-arguments>]"
    echo ""
    echo "-h               This help screen"
    echo "-t <trace-file>  The trace file to store to, if not set it will be echoed to stderr"
    echo "-o               Enable overwrite, this will always store to the same file, if unset it will write to <trace-file>.<pid>"
    echo ""
    echo "Needed tools and libs:"
    echo "libSegFault.so"
    echo "addr2line (optional)"
}

while getopts "oht:" opt; do
    case $opt in
    t)
        TRACEFILE=${OPTARG}
    ;;
    o)
        OVERWRITE=true
    ;;
    h)
        usage
        exit 0
    ;;
    esac
done
shift $(($OPTIND - 1))
PROGRAM=$1
if [ -z "${PROGRAM}" ]
then
    echo "ERROR: program name has to be set, try $0 -h for more information."
    exit 1
fi
PROGRAM_ARGS="${@:2}"

# check requirements
if [ -e "/usr/lib/libSegFault.so" ]
then
    LIBSEGFAULT="/usr/lib/libSegFault.so"
elif [ -e "/lib/libSegFault.so" ]
then
    LIBSEGFAULT="/lib/libSegFault.so"
fi

if [ "$LIBSEGFAULT" = "" ]
then
    "You need to have libSegFault.so either in /usr/lib or /lib installed"
    exit 1
fi

ADDR2LINE_EXISTS=$(command -v addr2line)

### run the program, this part is stolen from the catchsegv script since this script does not support to write its findings to somwhere else then stdout
# Redirect stderr to avoid termination message from shell.
prog=${PROGRAM}
segv_output=`mktemp ${TMPDIR:-/tmp}/segv_output.XXXXXX` || exit
(exec 3>&2 2>/dev/null
LD_PRELOAD=${LD_PRELOAD:+${LD_PRELOAD}:}${LIBSEGFAULT} \
SEGFAULT_USE_ALTSTACK=1 \
SEGFAULT_OUTPUT_NAME=$segv_output \
"$prog" ${PROGRAM_ARGS} 2>&3 3>&-)
exval=$?

CATCH_OUTPUT=""
### parse the output if necessary, again stolen from catchsegv script
if test -s "$segv_output"; then
  # The program caught a signal.  The output is in the file with the
  # name we have in SEGFAULT_OUTPUT_NAME.  In the output the names of
  # functions in shared objects are available, but names in the static
  # part of the program are not.  We use addr2line to get this information.
  case $prog in
  */*) ;;
  *)
    old_IFS=$IFS
    IFS=:
    for p in $PATH; do
      test -n "$p" || p=.
      if test -f "$p/$prog"; then
        prog=$p/$prog
      break
      fi
    done
    IFS=$old_IFS
    ;;
  esac
  CATCH_OUTPUT=$(sed '1,/Backtrace/d' "$segv_output" |
    (while read line; do
        line=`echo $line | sed "s@^$prog\\(\\[.*\\)@\1@"`
        output_line=""
        case "$line" in
        \[*) addr=`echo "$line" | sed 's/^\[\(.*\)\]$/\1/'`
                if [ "${ADDR2LINE_EXISTS}" = "" ]
                then
                    #does not exist, gives something like "[0x400497]" which means that you can use the addr2line manualy with the correct unstripped version as prog and the address inside the [] as addr
                    output_line="$line"
                else
                    #does exist
                    complete=`addr2line -f -e "$prog" $addr 2>/dev/null`
                    if test $? -eq 0; then
                        output_line="`echo "$complete"|sed 'N;s/\(.*\)\n\(.*\)/\2(\1)/;'`$line"
                    else
                        output_line="$line"
                    fi
                fi
                ;;
            *) output_line="$line"
                ;;
        esac
        echo $output_line
    done); printf x);
  CATCH_OUTPUT=${CATCH_OUTPUT%x}
fi
rm -f "$segv_output"

# finish if nothing was captured
if [ -z "${CATCH_OUTPUT}" ]
then
    exit $exval
fi

# collect additional information
function motdVersion()
{
    echo "TODO: getVersion of Image/Program/..."
}

NOW=$(date +"%F %H:%M:%S")
VERSION=$(motdVersion)
GENERATED_LINE="Generated on ${NOW} with version $VERSION\n\n"
DMESG_LAST_20=$(dmesg | tail -n 20; printf x); DMESG_LAST_20=${DMESG_LAST_20%x}
DMESG_LINES="-----------------------DMESG-----------------------\n${DMESG_LAST_20}-----------------------DMESG END-----------------------\n\n"
BACKTRACE_LINES="-----------------------BACKTRACE-----------------------\n${CATCH_OUTPUT}-----------------------BACKTRACE END-----------------------\n\n"


# print to file or console
OUTPUT=${GENERATED_LINE}"${DMESG_LINES}"${BACKTRACE_LINES} # the " are needed so that the \n inside the captured outputs will not get lost

OUTPUT_FILE=""
if [[ ! -z $TRACEFILE ]]
then
    if $OVERWRITE
    then
        OUTPUT_FILE=${TRACEFILE}
    else
        OUTPUT_FILE=${TRACEFILE}"."$$
    fi
fi

if [[ -z $OUTPUT_FILE ]]
then
    (>&2 echo -e "$OUTPUT")
else
    echo -e "$OUTPUT" > ${OUTPUT_FILE}
    echo "traclog written to "${OUTPUT_FILE}
fi

#finish with the retval from teh program
exit $exval
