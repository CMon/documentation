#include "MockSerialDevice.h"

#include <string.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>

void MockSerialDevice::init()
{
    fd_ = ::open("/dev/ptmx", O_RDWR);
    if (fd_ < 0) {
        throw MockSerialDeviceError("Cannot open /dev/ptmx: " + std::string(::strerror(errno)));
    }

    const char *sName = ptsname(fd_);
    if (!sName) {
        throw MockSerialDeviceError("ptsname failed: " + std::string(::strerror(errno)));
    }
    serialDevice_ = sName;

    struct stat s;
    int r = ::stat(serialDevice_.c_str(), &s);
    if (r != 0) {
        throw MockSerialDeviceError("Stat error: " + std::string(::strerror(errno)));
    }
    if (!S_ISCHR(s.st_mode)) {
        throw MockSerialDeviceError("Device is no character device");
    }

    r = grantpt(fd_);
    if (r != 0) {
        throw MockSerialDeviceError("grantpt failed: " + std::string(::strerror(errno)));
    }

    r = unlockpt(fd_);
    if (r != 0) {
        throw MockSerialDeviceError("unlockpt failed: " + std::string(::strerror(errno)));
    }
}

void MockSerialDevice::loop()
{
    ssize_t available = 0;
    int ret = ::ioctl(fd_, FIONREAD, (char *) &available);
    if (ret != 0)
        return;

    std::vector<uint8_t> buffer;
    buffer.reserve(available);
    ssize_t got = ::read(fd_, buffer.data(), available);
    if (got < 0) {
        throw MockSerialDeviceError("Terminal: Read failed: " + std::string(::strerror(errno)));
    }
    buffer.resize(got);

    std::lock_guard<std::mutex> guard(readDataAccess_);
    readData_.insert(readData_.end(), buffer.begin(), buffer.end());
}

void MockSerialDevice::close()
{
    ::close(fd_);
}

void MockSerialDevice::setTemios(termios &newTermIOs, speed_t baudrate, bool setISpeed, bool setOSpeed)
{
    if(setISpeed && ::cfsetispeed(&newTermIOs, baudrate) != 0) {
        throw MockSerialDeviceError("cfsetispeed error: " + std::string(::strerror(errno)));
    }
    if(setOSpeed && ::cfsetospeed(&newTermIOs, baudrate) != 0) {
        throw MockSerialDeviceError("cfsetospeed error: " + std::string(::strerror(errno)));
    }

    // discard any previous data
    if(::tcflush(fd_, TCIFLUSH) != 0) {
        throw MockSerialDeviceError("tcflush error: " + std::string(::strerror(errno)));
    }

    if(::tcsetattr(fd_, TCSANOW, &newTermIOs) != 0) {
        throw MockSerialDeviceError("tcsetattr error: " + std::string(::strerror(errno)));
    }
}

std::string MockSerialDevice::serialDevice() const
{
    return serialDevice_;
}

void MockSerialDevice::addSuffixForWriting(const std::string &suffix)
{
    suffix_ = suffix;
}

void MockSerialDevice::write(const std::vector<std::uint8_t> &data)
{
    write(data.data(), (ssize_t)data.size());
}

void MockSerialDevice::write(const std::string &data)
{
    write((std::uint8_t *)data.c_str(), (ssize_t)data.size());
}

bool MockSerialDevice::dataAvailable()
{
    std::lock_guard<std::mutex> guard(readDataAccess_);
    return readData_.size() > 0;
}

std::vector<std::uint8_t> MockSerialDevice::readAll()
{
    std::lock_guard<std::mutex> guard(readDataAccess_);
    std::vector<std::uint8_t> retBuffer = readData_;
    readData_.clear();
    return retBuffer;
}

void MockSerialDevice::write(const std::uint8_t *data, ssize_t size)
{
    auto ret = ::write(fd_, data, size);
    if(ret != size) {
        throw MockSerialDeviceError("Error writing data (" +std::to_string(ret)+  "/" +std::to_string(size)+  "): " + std::string(::strerror(errno)));
    }

    if(suffix_.size() > 0) {
        auto ret = ::write(fd_, suffix_.data(), suffix_.size());
        if(ret != suffix_.size()) {
            throw MockSerialDeviceError("Error writing data (" +std::to_string(ret)+  "/" +std::to_string(suffix_.size())+  "): " + std::string(::strerror(errno)));
        }
    }

    if(::tcdrain(fd_) != 0) {
        throw MockSerialDeviceError("tcdrain error: " + std::string(::strerror(errno)));
    }
}
