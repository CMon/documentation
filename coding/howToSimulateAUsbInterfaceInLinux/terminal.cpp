#include "terminal.h"

#include <QDebug>
#include <QIODevice>
#include <QSocketNotifier>

#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/stat.h>

static QString currentError()
{
    int err = errno;
    return QString::fromLatin1(strerror(err));
}

Terminal::Terminal(QObject *parent)
   : QObject(parent), m_isUsable(false), m_masterFd(-1), m_masterReader(0)
{
}

bool Terminal::setup()
{
    m_masterFd = ::open("/dev/ptmx", O_RDWR);
    if (m_masterFd < 0) {
        qCritical () << QString("Cannot open /dev/ptmx: %1").arg(currentError());
        return false;
    }

    const char *sName = ptsname(m_masterFd);
    if (!sName) {
        qCritical () << QString("ptsname failed: %1").arg(currentError());
        return false;
    }
    m_slaveName = sName;

    struct stat s;
    int r = ::stat(m_slaveName.constData(), &s);
    if (r != 0) {
        qCritical () << QString("Error: %1").arg(currentError());
        return false;
    }
    if (!S_ISCHR(s.st_mode)) {
        qCritical () << QString("Slave is no character device.");
        return false;
    }

    m_masterReader = new QSocketNotifier(m_masterFd, QSocketNotifier::Read, this);
    connect(m_masterReader, &QSocketNotifier::activated, this, &TestUARTGateway::onSlaveReaderActivated);

    r = grantpt(m_masterFd);
    if (r != 0) {
        qCritical () << QString("grantpt failed: %1").arg(currentError());
        return false;
    }

    r = unlockpt(m_masterFd);
    if (r != 0) {
        qCritical () << QString("unlock failed: %1").arg(currentError());
        return false;
    }

    return true;
}

int Terminal::write(const QByteArray &msg)
{
    return ::write(m_masterFd, msg.constData(), msg.size());
}

bool Terminal::sendInterrupt()
{
    if (!m_isUsable)
        return false;
    ssize_t written = ::write(m_masterFd, "\003", 1);
    return written == 1;
}

void Terminal::onSlaveReaderActivated(int fd)
{
    ssize_t available = 0;
    int ret = ::ioctl(fd, FIONREAD, (char *) &available);
    if (ret != 0)
        return;

    QByteArray buffer(available, Qt::Uninitialized);
    ssize_t got = ::read(fd, buffer.data(), available);
    int err = errno;
    if (got < 0) {
        error(tr("Terminal: Read failed: %1").arg(QString::fromLatin1(strerror(err))));
        return;
    }
    buffer.resize(got);

    emit newData(buffer)
}
