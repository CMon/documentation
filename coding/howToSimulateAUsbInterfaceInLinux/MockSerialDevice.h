#ifndef MOCKSERIALDEVICE_H
#define MOCKSERIALDEVICE_H

#include <vector>
#include <string>
#include <mutex>
#include <stdexcept>
#include <termios.h>

class MockSerialDeviceError : public std::runtime_error
{
public:
    MockSerialDeviceError(const std::string& s): std::runtime_error(s) { }
};

class MockSerialDevice
{
public:
    MockSerialDevice() = default;

    /**
     * @brief init the Serial device, this will open the new ptDevice
     * @throws MockSerialDeviceError
     */
    void init();

    /**
     * @brief loop This method has to be called regularly if you want to read data back
     * @throws MockSerialDeviceError if read fails even if data was available
     */
    void loop();

    /**
     * @brief close the sockets
     */
    void close();

    /**
     * @brief do not use the default setting of the terminal
     * @param newTermIOs
     * @param baudrate
     * @param setispeed
     * @param setospeed
     * @throws MockSerialDeviceError if one of the setters fails
     */
    void setTemios(struct termios& newTermIOs, speed_t baudrate,  bool setISpeed, bool setOSpeed);

    /**
     * @brief returns the fake serial device name
     * @return the fake serial device
     */
    std::string serialDevice() const;

    /**
     * @brief add a suffix that is always appended to each write, for example '\n' or '\r\n'
     * @param suffix The suffix
     */
    void addSuffixForWriting(const std::string& suffix);

    /**
     * @brief write data through the serial device
     * @param data the data to send
     * @throws MockSerialDeviceError if the write had an error or did not send enough data
     */
    void write(const std::vector<std::uint8_t> & data);

    /**
     * @brief write data through the serial device
     * @param the string to send
     * @throws MockSerialDeviceError if the write had an error or did not send enough data
     */
    void write(const std::string& data);

    /**
     * @brief chekc if read data is available
     * @return true if data is available, false if not
     */
    bool dataAvailable();

    /**
     * @brief takes all data from the read buffer and clears it
     * @return the read data
     */
    std::vector<std::uint8_t> readAll();

private:
    void write(const std::uint8_t * data, ssize_t size);

private:
    int fd_;
    std::string serialDevice_;
    std::string suffix_;

    std::mutex readDataAccess_;
    std::vector<uint8_t> readData_;
};

#endif // MOCKSERIALDEVICE_H
