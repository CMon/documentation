= Goal is to create an encrypted backup on a harddrive 
to have the most important stuff as an offline backup

.Install needed software
----
sudo pacman -Sy borg
----

Basic strucutre of a borg backup is a repository, with different snapshots of the packed up data in it, the backup solution uses deduplication of the encrypted blocks, therefore even a new backup, might use less storage than the origin (as long as the origin is not on a deduplication fs)

So to use a borg backup you need to first create its repository:
----
borg init --encryption=repokey /path/to/repo
----
This call will ask for the password, that will be used for the backup

After the creation you can add sources to a snapshot with the following call:
----
borg create --stats /path/to/repo::Monday ~/src ~/Documents
----

To have a list of snapshots in a repository run:
----
borg list /path/to/repo
----

To list files inside a snapshot run:
----
borg list /path/to/repo::Monday
----

to extract all files from one snapshot, relatively to the current location, run:
----
borg extract /path/to/repo::Monday
----

to extract just one file from a snapshot:
----
borg extract /path/to/repo::Monday path/of/file/like/in/list/command
----

to remove an snapshot run:
----
borg delete /path/to/repo::Monday
----
