require 'asciidoctor'
require 'asciidoctor/extensions'
require 'securerandom'

#heavily inspired from: https://github.com/Alwinator/asciidoctor-lists/blob/main/lib/asciidoctor-lists/extensions.rb

module AsciidoctorListOf
    module Asciidoctor

        ListRoleMacroAttributes = Hash.new

        # Replaces list-of::element[] with UUID and saves attributes in ListMacroPlaceholder
        class ListRoleMacro < ::Asciidoctor::Extensions::BlockMacroProcessor
            use_dsl
            named :"list-of-role"

            def process(parent, target, attrs)
                uuid = SecureRandom.uuid
                ListRoleMacroAttributes[uuid] = {
                    element: target
                }
                create_paragraph parent, uuid, {}
            end
        end

        class ListRoleTreeprocessor < ::Asciidoctor::Extensions::Treeprocessor
            def process(document)
                tof_blocks = document.find_by do |b|
                    # for fast search (since most searches shall fail)
                    (b.content_model == :simple) && (b.lines.size == 1) \
                    && (ListRoleMacroAttributes.keys.include?(b.lines[0]))
                end
                tof_blocks.each do |block|
                    references_asciidoc = []
        
                    params = ListRoleMacroAttributes[block.lines[0]]
                    starts_with = params[:starts_with]

                    elements = document.find_by(traverse_documents: true, role: params[:element])
                    if elements.length > 0
                        elements.each do |element|
                            if element.id
                                if element.alt
                                    references_asciidoc << %(* xref:#{element.id}[#{element.id}] - #{element.alt.rstrip()} +)
                                else
                                    references_asciidoc << %(* xref:#{element.id}[#{element.id}] +)
                                end
                            end
                        end
                    end

                    block_index = block.parent.blocks.index do |b|
                        b == block
                    end

                    reference_blocks = parse_asciidoc block.parent, references_asciidoc
                    reference_blocks.reverse.each do |b|
                        block.parent.blocks.insert block_index, b
                    end
                    block.parent.blocks.delete_at block_index + reference_blocks.size
                end
            end
             # This is an adapted version of Asciidoctor::Extension::parse_content,
             # where resultant blocks are returned as a list instead of attached to
             # the parent.
            def parse_asciidoc(parent, content, attributes = {})
                result = []
                reader = ::Asciidoctor::Reader.new content
                while reader.has_more_lines?
                    block = ::Asciidoctor::Parser.next_block reader, parent, attributes
                    result << block if block
                end
                result
            end
        end
    end
end

# Register the extensions to asciidoctor
Asciidoctor::Extensions.register do
    block_macro AsciidoctorListOf::Asciidoctor::ListRoleMacro
    tree_processor AsciidoctorListOf::Asciidoctor::ListRoleTreeprocessor
end
