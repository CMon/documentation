module.exports = function (registry) {
    registry.inlineMacro('gitlab', function () {
      var self = this
      self.process(function (parent, target) {
        var text
        if (target === 'issue') {
          text = getGlobalVariable("gitlabIssueLinkBase") + attribute + "[" + attribute + "]"
        } else if (target === 'mr') {
          text = getGlobalVariable("gitlabMergeRequestLinkBase") + attribute + "[" + attribute + "]"
        } else {
            return ""
        }
        return self.createInline(parent, 'quoted', text, { 'type': 'strong' })
      })
    })
  }
