require 'asciidoctor'
require 'asciidoctor/extensions'

class ReqInlineMacro < Asciidoctor::Extensions::InlineMacroProcessor
    include Asciidoctor::Logging
    use_dsl
  
    named :"req"
    name_positional_attributes 'linkText'
  
    def process parent, target, attrs
      doc = parent.document
      logger.info "Linking Requirement: " + target

      if doc.attributes['use-req'] == "doc"
        link = "xref:" + doc.attributes['req-doc'] + "#" + target + "[" +attrs['linkText']+ "]"
      else
        link = doc.attributes['req-url'] + target + "[" +attrs['linkText']+ "]"
      end
      create_inline parent, :quoted, link
    end
  end


# Register the extensions to asciidoctor
Asciidoctor::Extensions.register do
    inline_macro ReqInlineMacro
end
