#!/bin/bash

SOURCE=/tmp/gitlab-move-test-source
TARGET=/tmp/gitlab-move-test-target
NUMBER_OF_FILES=10
ll='ls -lF'

#rsync stuff
# typical: -avz
# --archive, -a            archive mode is -rlptgoD (no -A,-X,-U,-N,-H)
# --verbose, -v            increase verbosity
# --compress, -z           compress file data during the transfer
# --delete                 delete extraneous files from dest dirs

RSYNC_ARGS=" -avz --delete $SOURCE/ $TARGET"

echo "creating structure"

rm -rf $SOURCE
rm -rf $TARGET
rm -f /tmp/*-source-*.log
rm -f /tmp/*-target-*.log

mkdir -p $SOURCE/gitlab/foo
mkdir -p $SOURCE/gitlab/bar
mkdir -p $SOURCE/gitlab/old

for f in `seq 0 ${NUMBER_OF_FILES}`
do
    echo "file_$f" >> $SOURCE/gitlab/foo/foo_$f
done

for f in `seq 0 ${NUMBER_OF_FILES}`
do
    file=$SOURCE/gitlab/bar/bar_$f
    echo "file_$f" > $file
    chgrp docker $file
done

for f in `seq 0 ${NUMBER_OF_FILES}`
do
    file=$SOURCE/gitlab/old/old_$f
    echo "file_$f" > $file
    touch -t 202403251234 $file
done

$ll $SOURCE/* -R > /tmp/01-source-created.log

echo "running rsync to target"
rsync ${RSYNC_ARGS}
$ll $TARGET/* -R > /tmp/01-target-synced.log



echo "removing some source files, adding new ones"

rm /tmp/gitlab-move-test-source/gitlab/foo/foo_2
rm /tmp/gitlab-move-test-source/gitlab/foo/foo_5
rm /tmp/gitlab-move-test-source/gitlab/bar/bar_1
rm /tmp/gitlab-move-test-source/gitlab/bar/bar_7
rm /tmp/gitlab-move-test-source/gitlab/old/old_10
rm /tmp/gitlab-move-test-source/gitlab/old/old_3

echo "new one" > /tmp/gitlab-move-test-source/gitlab/foo/foo_13
echo "new one" > /tmp/gitlab-move-test-source/gitlab/bar/noenoe_007
$ll $SOURCE/* -R > /tmp/02-source-created.log


echo running rsync to target
rsync ${RSYNC_ARGS}
$ll $TARGET/* -R > /tmp/02-target-synced.log
