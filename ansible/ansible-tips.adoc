= Using systemd task

.Example systemd task
----
  - name: enable »signer« systemd service
    become_user: signer
    become_method: su
    become: true
    systemd:
      daemon_reload: yes
      enabled: yes
      masked: no
      name: p11-kit-client
      scope: user
      state: restarted
----

If you have to run a systemd task as a user you might run into the problem that the systemd call responds with: `Failed to connect to bus: Permission denied`, to fix it you need to change the become_method to `machinectl` and probably need to install its matching package: `systemd-container`(debian). The reason is that the `become_method: su` does not do a real login and therefore has no runnig services.

.Working example:
----
  - name: enable »signer« systemd service
    become_user: signer
    become_method: machinectl <1>
    become: true
    systemd:
      daemon_reload: yes
      enabled: yes
      masked: no
      name: p11-kit-client
      scope: user
      state: restarted
----
<1> This could be configured in the global scope as well
