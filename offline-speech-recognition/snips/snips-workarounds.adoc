= Tips & Tricks around Snips (snips.ai)

== Bug workarounds

.On a recent install (0.63.3) and stretch from 09.2019 the audio out through the lineOut does not work
. You can verify it by asking snips something and getting no answer, with in the journal of the system `journalctl -f` you get port audio errors of wrong sampling rates
.. The ticket regarding this is here: https://github.com/snipsco/snips-issues/issues/148
. Most of the workarounds in the first post do not work, but `HakanL` had suggestion for the environment that worked:
.. Edit `/etc/systemd/system/multi-user.target.wants/snips-audio-server.service` and add a Environment variable:
----
[Service]
Environment="PA_ALSA_PLUGHW=1"
----
. restart snips with `sam reboot`
