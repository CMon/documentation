#!/usr/bin/env python
import yaml
import getpass
import argparse
import pprint
from functions import getLayout, getVariables, getBaseUrl, getFiles, getCurlRequests

pp = pprint.PrettyPrinter(width=200)

parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("-c", "--configFile", help="path to configFile", default="./config.yaml")
parser.add_argument("-f", "--filesDir", help="folder on where to search the files, FULL PATH not relative", default="./")
args = parser.parse_args()

with open(args.configFile, 'r') as f:
    config = yaml.safe_load(f)

# get the password if its missing
password = config["artifactServer"]["password"]
if password is None or len(password) == 0:
    password = getpass.getpass(f"Nexus password for user {config['artifactServer']['username']}: ")

# get the layout to use
layout = getLayout(config)

# create baseUrl
baseUrl = getBaseUrl(config, layout, getVariables(config, layout))

# get the files
files = getFiles(config, layout, args.filesDir)

# print a summery on what files go where
curlRequests = getCurlRequests(config, baseUrl, args.filesDir, files)

print("Are the following requests good??\n")
for request in curlRequests:
    print(request)

# after permission granted, upload files
value = input("Is this ok? [y/N] ")
if value == "y" or value == "Y":
    curlRequests = getCurlRequests(config, baseUrl, args.filesDir, files, password)
    print("Run the following requests from inside a safe env without history (would store the password)")
    for request in curlRequests:
        print(request)
else:
    print("aborted")
