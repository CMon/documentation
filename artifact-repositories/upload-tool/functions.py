import glob
import urllib.parse
from consolemenu import SelectionMenu

def getLayout(config):
    layouts = []
    for layout in config["uploadLayout"]:
        layouts.append(layout)

    layoutSelection = SelectionMenu.get_selection(layouts)
    if layoutSelection == len(layouts):
        return None
    return layouts[layoutSelection]

def getVariables(config, layoutName):
    variables = config["uploadLayout"][layoutName]["askFor"]
    retvars = {}
    for var in variables:
        value = input(f"Enter a value for {var}: ")
        retvars[var] = value

    return retvars

def getBaseUrl(config, layoutName, variables):
    newBase = config["uploadLayout"][layoutName]["basepath"]
    for k, v in variables.items():
        newBase = newBase.replace(f"%%{k}%%", v)
    url = config["artifactServer"]["url"] + urllib.parse.quote(newBase)
    return url

def getFiles(config, layoutName, filesDir):
    artifacts = {}
    for artifact in config["uploadLayout"][layoutName]["artifacts"]:
        folder = artifact["folder"]
        artifacts[folder] = []
        for fileFilter in artifact["files"]:
            files = glob.glob(fileFilter, root_dir=filesDir)
            artifacts[folder].extend(files)
    return artifacts

def getCurlRequests(config, baseUrl, filesDir, files, password = "****"):
    curlOpts = ["--location"] # follow a redirect and redo the request if necessary
    if config["artifactServer"]["insecure"] == True:
        curlOpts.append("--insecure")
    username = config["artifactServer"]["username"]

    curlCommands = []
    for subFolder, subFiles in files.items():
        for file in subFiles:
            target = baseUrl + "/" + urllib.parse.quote(subFolder + "/" + file)
            curlCommand = f"curl {' '.join(curlOpts)} --user \"{username}:{password}\" --upload-file \"{filesDir}/{file}\" \"{target}\""
            curlCommands.append(curlCommand)
    return curlCommands
