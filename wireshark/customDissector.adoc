= Writing a custom dissector (interpreter for a protocol)

Create a lua script that utilizes the dissector API inside `~/.local/lib/wireshark/plugins/`. Or make a link to a file in a repository in this folder.

.Example: `ATO-dissector.lua`
----
include::customDissector/ATO-dissector.lua[]
----

.Better Example: `CAFVLan-dumpFile-dissector.lua`
----
include::customDissector/CAFVLan-dumpFile-dissector.lua[]
----

during development use kbd:[CTRL+SHIFT+L] to reload the lua script, best to have already captured data. Then the interpretion would directly take effect.
