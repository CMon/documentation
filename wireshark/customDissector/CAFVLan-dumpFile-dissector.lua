protoCAFVLanDumpFiles = Proto("CAFVLanDumpFiles", "CAFVLan DumpFiles protocol")

head_version = ProtoField.uint8("head_version", "Version", base.HEX)

local S_ApE_JRU_alarm = 4
local S_JRU_ApE_alarm_ack = 14

local msgIdValues = {
  [S_ApE_JRU_alarm] = "S_ApE_JRU_alarm",
  [S_JRU_ApE_alarm_ack] = "S_JRU_ApE_alarm_ack"
}
head_msgId = ProtoField.uint8("head_msgId", "MSGID", base.HEX, msgIdValues)

local fileValues = {[0] = "alarm", [1] = "dump_sil4", [2] = "dump_sil4_local", [3] = "dump_sil2", [4] = "dump_sil2_local", [5] = "dump_sil0", [6] = "dump_sil0_local"}
head_dump_nFile = ProtoField.uint8("head_dump_nFile", "File Content ID", base.HEX, fileValues)

head_dump_ChannelTypeFlags = ProtoField.uint8("head_dump_ChannelTypeFlags", "EVC Channel, Type, Flags", base.HEX)
head_dump_nChannel = ProtoField.uint8("head_dump_nChannel", "EVC Channel", base.DEC, nil, 0xC0)

local typeValues = {[0] = "Dump file", [1] = "Alarm file"}
head_dump_nType = ProtoField.uint8("head_dump_nType", "Type", base.DEC, typeValues, 0x3C)

local flagsValues = {[0] = "No more fragments", [1] = "More fragments"}
head_dump_nFlags = ProtoField.uint8("head_dump_nFlags", "Flags", base.DEC, flagsValues, 0x03)

head_dump_nFragmentOffset = ProtoField.uint32("head_dump_nFragmentOffset", "Offset", base.DEC)
head_dump_lMsg = ProtoField.uint16("head_dump_lMsg", "Length of payload", base.DEC)

payload = ProtoField.bytes("payload", "Payload")
local ackNackValues = {[0x06] = "ACK", [0x16] = "NACK"}
payload_acknack = ProtoField.uint8("payload_acknack", "Payload", base.HEX, ackNackValues)

protoCAFVLanDumpFiles.fields = {
  head_version
, head_msgId

, head_dump_nFile
, head_dump_ChannelTypeFlags
, head_dump_nChannel
, head_dump_nType
, head_dump_nFlags
, head_dump_nFragmentOffset
, head_dump_lMsg

, payload
, payload_acknack
}

function strVersion(version)
    local major = bit.rshift(bit.band(version, 0xF0), 4)
    local minor = bit.band(version, 0x0F)
    return " (" .. major .. "." .. minor .. ")"
end

function protoCAFVLanDumpFiles.dissector(buffer, pinfo, tree)
    length = buffer:len()
    if length == 0 then
        return
    end

    pinfo.cols.protocol = protoCAFVLanDumpFiles.name

    local subtree = tree:add(protoCAFVLanDumpFiles, buffer(), "CAF DumpFiles Protocol Data")
    local headerSubTree = subtree:add(protoCAFVLanDumpFiles, buffer(), "Header")
    local payloadSubTree = subtree:add(protoCAFVLanDumpFiles, buffer(), "Payload (with padding)")

    local bufferIndex = 0

    local version = buffer(bufferIndex,1):uint(); bufferIndex = bufferIndex + 1
    headerSubTree:add(head_version, version):append_text(strVersion(version))
    local msgId = buffer(bufferIndex,1):uint(); bufferIndex = bufferIndex + 1
    headerSubTree:add(head_msgId, msgId)
    if msgId == S_ApE_JRU_alarm then
        local nFile = buffer(bufferIndex,1):uint(); bufferIndex = bufferIndex + 1
        headerSubTree:add(head_dump_nFile, nFile)
    end
    local nChannelTypeFlags = buffer(bufferIndex,1):uint(); bufferIndex = bufferIndex + 1
    local ctf = headerSubTree:add(head_dump_ChannelTypeFlags, nChannelTypeFlags)
    ctf:add(head_dump_nChannel, nChannelTypeFlags)
    ctf:add(head_dump_nType, nChannelTypeFlags)
    ctf:add(head_dump_nFlags, nChannelTypeFlags)

    local nFragmentOffset = buffer(bufferIndex,4):le_uint(); bufferIndex = bufferIndex + 4
    headerSubTree:add(head_dump_nFragmentOffset, nFragmentOffset)

    if msgId == S_ApE_JRU_alarm then
        local lMsg = buffer(bufferIndex,2):le_uint(); bufferIndex = bufferIndex + 2
        local nPayload = buffer(bufferIndex)

        headerSubTree:add(head_dump_lMsg, lMsg)
        payloadSubTree:add(payload, nPayload)
    elseif msgId == S_JRU_ApE_alarm_ack then
        local nPayload = buffer(bufferIndex,1); bufferIndex = bufferIndex + 1
        payloadSubTree:add(payload_acknack, nPayload)
    end
end

local vlan_type_filter = DissectorTable.get("ethertype")
vlan_type_filter:add(0xcaf8, protoCAFVLanDumpFiles)
