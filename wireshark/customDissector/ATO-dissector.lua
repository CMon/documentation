proto_ato = Proto("ATO_01_07",  "ATO Protocol Packets 01 and 07 from Subset 130")

packet_number = ProtoField.uint8("ATO_packet_number", "Packet Number", base.DEC)
packet_length = ProtoField.uint16("ATO_packet_legnth", "Length", base.DEC)
packet_timestamp = ProtoField.relative_time("ATO_packet_timestamp", "Timestamp")

packet1_ato_dmi_info = ProtoField.uint16("ATO_1_ato_dmi_info", "DMI Indicators BITSET", base.DEC)

-- wrong masks used in dsh
wrong_atoDmiInfo_ato_status     = ProtoField.uint16("ato_dmi_info_ato_status", "M_ATOSTATUS", base.DEC, nil, 0x0007)
wrong_atoDmiInfo_stop_accuracy  = ProtoField.uint16("ato_dmi_info_stop_accuracy", "Q_STOPACCURACY", base.DEC, nil, 0x0018)
wrong_atoDmiInfo_dwelltime_info = ProtoField.uint16("ato_dmi_info_dwelltime_info", "Q_DWELLTIME_INFO", base.DEC, nil, 0x0060)
wrong_atoDmiInfo_doorinfo       = ProtoField.uint16("ato_dmi_info_doorinfo", "Q_DOORINFO", base.DEC, nil, 0x0380)
wrong_atoDmiInfo_skipstp        = ProtoField.uint16("ato_dmi_info_skipstp", "Q_SKIPSTP", base.DEC, nil, 0x0C00)
wrong_atoDmiInfo_coasting       = ProtoField.uint16("ato_dmi_info_coasting", "Q_COASTING", base.DEC, nil, 0x1000)
wrong_atoDmiInfo_warningsound   = ProtoField.uint16("ato_dmi_info_warningsound", "Q_WARNINGSOUND", base.DEC, nil, 0x2000)
wrong_atoDmiInfo_spare          = ProtoField.uint16("ato_dmi_info_spare", "Spare", base.DEC, nil, 0xC000)
--correct masks for used in non dsh
atoDmiInfo_ato_status     = ProtoField.uint16("ato_dmi_info_ato_status", "M_ATOSTATUS", base.DEC, nil, 0xE000)
atoDmiInfo_stop_accuracy  = ProtoField.uint16("ato_dmi_info_stop_accuracy", "Q_STOPACCURACY", base.DEC, nil, 0x1800)
atoDmiInfo_dwelltime_info = ProtoField.uint16("ato_dmi_info_dwelltime_info", "Q_DWELLTIME_INFO", base.DEC, nil, 0x0600)
atoDmiInfo_doorinfo       = ProtoField.uint16("ato_dmi_info_doorinfo", "Q_DOORINFO", base.DEC, nil, 0x01C0)
atoDmiInfo_skipstp        = ProtoField.uint16("ato_dmi_info_skipstp", "Q_SKIPSTP", base.DEC, nil, 0x0030)
atoDmiInfo_coasting       = ProtoField.uint16("ato_dmi_info_coasting", "Q_COASTING", base.DEC, nil, 0x0008)
atoDmiInfo_warningsound   = ProtoField.uint16("ato_dmi_info_warningsound", "Q_WARNINGSOUND", base.DEC, nil, 0x0004)
atoDmiInfo_spare          = ProtoField.uint16("ato_dmi_info_spare", "Spare", base.DEC, nil, 0x0003)

packet1_t_dwelltime = ProtoField.uint16("ATO_1_t_dwelltime", "Remaining Dwell Time (s)", base.DEC)
packet1_v_tas = ProtoField.uint16("ATO_1_v_tas", "Target Advice Speed (cm/s)", base.DEC)
packet1_d_nextadvice = ProtoField.uint32("ATO_1_d_nextadvice", "Distance to next advice change (cm)", base.DEC)
packet1_t_next_stp_arrival_time = ProtoField.uint32("ATO_1_t_next_stp_arrival_time", "Arrival time to the next Stopping Point (s)", base.DEC)
packet1_l_text_stp = ProtoField.uint8("ATO_1_l_text_stp", "Length of text next Stopping Point", base.DEC)
packet1_x_text_stp = ProtoField.string("ATO_1_x_text_stp", "Name of the next Stopping Point", base.ASCII)
packet1_n_stpdistance_iter = ProtoField.uint8("ATO_1_n_stpdistance_iter", "Number of distance to the next Stopping Point", base.DEC)
packet1_d_stpdistance = ProtoField.uint32("ATO_1_d_stpdistance", "Distance to the Stopping Point or Stopping Point (cm)", base.DEC)

packet7_n_atoengage_selection = ProtoField.uint8("ATO_7_n_atoengage", "ATO Engage selection counter", base.DEC)
packet7_skipstpreq_selection = ProtoField.uint8("ATO_7_skipstpreq", "Skip Stopping Point Request selection counter", base.DEC)
packet7_skipstprev_selection = ProtoField.uint8("ATO_7_skipstprev", "Skip Stopping Point Revocation selection counter", base.DEC)

proto_ato.fields = {
      packet_number
    , packet_length
    , packet_timestamp
    , info_bytes

    , packet1_ato_dmi_info
    , wrong_atoDmiInfo_ato_status
    , wrong_atoDmiInfo_stop_accuracy
    , wrong_atoDmiInfo_dwelltime_info
    , wrong_atoDmiInfo_doorinfo      
    , wrong_atoDmiInfo_skipstp       
    , wrong_atoDmiInfo_coasting      
    , wrong_atoDmiInfo_warningsound  
    , wrong_atoDmiInfo_spare         
    , atoDmiInfo_ato_status
    , atoDmiInfo_stop_accuracy
    , atoDmiInfo_dwelltime_info
    , atoDmiInfo_doorinfo      
    , atoDmiInfo_skipstp       
    , atoDmiInfo_coasting      
    , atoDmiInfo_warningsound  
    , atoDmiInfo_spare         

    , packet1_t_dwelltime
    , packet1_v_tas
    , packet1_d_nextadvice
    , packet1_t_next_stp_arrival_time
    , packet1_l_text_stp
    , packet1_x_text_stp
    , packet1_n_stpdistance_iter
    , packet1_d_stpdistance

    , packet7_n_atoengage_selection
    , packet7_skipstpreq_selection
    , packet7_skipstprev_selection
}

function getPacketNumberName(packetNumber)
   local name = "Unknown" 
   
   if packetNumber == 1 then name = "ATO_ETCS_DMI"
   elseif packetNumber == 7 then name = "ETCS_ATO_Driver_Inputs"
   end
   
   return name
end

function getDwellTimeText(dwelltime)
    if dwelltime == 65535 then
        return " (no Remaining Dwell Time indication)"
    end
    return ""
end

function getVTasText(vtas)
    if vtas >= 6001 and vtas <= 65534 then
        return " (spare)"
    elseif vtas == 65535 then
        return " (No Target Advice Speed indication)"
    end
    return ""
end

function getDistance(distanceInCM)
    local meters = distanceInCM / 100
    local kmeters = meters / 1000
    return string.format(" (=> %dm => %dkm)", meters, kmeters)
end

function getDNextAdviceText(d_nextAdvice)
    if d_nextAdvice == 0xFFFFFFFF then
        return " (No information)"
    end
    return getDistance(d_nextAdvice)
end

function getNextStopText(nextStop)
    if nextStop >= 86400 and nextStop <= 0xFFFFFFFE then
        return " (Spare)"
    elseif nextStop == 0xFFFFFFFF then
        return " (No information)"
    end
    return getDistance(nextStop)
end

function splitString(str)
    list = {}
    for i in string.gmatch(str, "%S+")
    do
        table.insert(list, i)
    end
    return list
end

function getAtoStatusText(field, value)
    -- ugly but no other way to get the mask back, since there is no getter of it
    mask = tonumber("0x" .. splitString(tostring(field))[7])
    statusVal = bit.band(value, mask)
    if     statusVal == 0x0 then return " (ATO Selected)"
    elseif statusVal == 0x1 then return " (ATO ready for engagement)"
    elseif statusVal == 0x2 then return " (ATO Engaged)"
    elseif statusVal == 0x3 then return " (ATO Disengaging)"
    elseif statusVal == 0x4 then return " (ATO Failure)"
    end
    
    return "(Spare)"
end

function proto_ato.dissector(buffer, pinfo, tree)
    length = buffer:len()
    if length == 0 then 
        return 
    end

    pinfo.cols.protocol = proto_ato.name

    local subtree = tree:add(proto_ato, buffer(), "ATO Protocol Data")
    local headerSubTree = subtree:add(proto_ato, buffer(), "Header")
    local payloadSubTree = subtree:add(proto_ato, buffer(), "Payload")

    local packetNumber = buffer(0,1):uint()
    local length = buffer(1,2):uint()
    local dataLength = length - 7 -- header is always 7 bytes long

    headerSubTree:add(packet_number, packetNumber):append_text(" (" .. getPacketNumberName(packetNumber) .. ")")
    headerSubTree:add(packet_length, length)
    
    local timestampInMs = buffer(3,4):uint()
    local timestampSPart = timestampInMs / 1000
    local timestampMsPart = timestampInMs % 1000
    local nstime = NSTime.new(timestampSPart, timestampMsPart * 1000 * 1000) -- second part is in nanoseconds
    headerSubTree:add(packet_timestamp, buffer(3,4), nstime) -- highlight 3,4 but with the new time
  
    if packetNumber == 1 then
        local pos = 7
        
        -- show both interpretations dsh and std.
        local dmiInfo = buffer(pos,2):uint()
        dmiInfoSubWrong = payloadSubTree:add(packet1_ato_dmi_info, buffer(pos,2)):append_text(" (DSH interpretation)");
        dmiInfoSubWrong:add(wrong_atoDmiInfo_ato_status, dmiInfo):append_text(getAtoStatusText(wrong_atoDmiInfo_ato_status, dmiInfo))
        dmiInfoSubWrong:add(wrong_atoDmiInfo_stop_accuracy, dmiInfo)
        dmiInfoSubWrong:add(wrong_atoDmiInfo_dwelltime_info, dmiInfo)
        dmiInfoSubWrong:add(wrong_atoDmiInfo_doorinfo, dmiInfo)
        dmiInfoSubWrong:add(wrong_atoDmiInfo_skipstp, dmiInfo)
        dmiInfoSubWrong:add(wrong_atoDmiInfo_coasting, dmiInfo)
        dmiInfoSubWrong:add(wrong_atoDmiInfo_warningsound, dmiInfo)
        dmiInfoSubWrong:add(wrong_atoDmiInfo_spare, dmiInfo)

        dmiInfoSub = payloadSubTree:add(packet1_ato_dmi_info, buffer(pos,2)):append_text(" (Correct interpretation)");
        dmiInfoSub:add(atoDmiInfo_ato_status, dmiInfo):append_text(getAtoStatusText(atoDmiInfo_ato_status, dmiInfo))
        dmiInfoSub:add(atoDmiInfo_stop_accuracy, dmiInfo)
        dmiInfoSub:add(atoDmiInfo_dwelltime_info, dmiInfo)
        dmiInfoSub:add(atoDmiInfo_doorinfo, dmiInfo)
        dmiInfoSub:add(atoDmiInfo_skipstp, dmiInfo)
        dmiInfoSub:add(atoDmiInfo_coasting, dmiInfo)
        dmiInfoSub:add(atoDmiInfo_warningsound, dmiInfo)
        dmiInfoSub:add(atoDmiInfo_spare, dmiInfo)
        pos = pos + 2
        
        local dwellTime = buffer(pos,2):uint()
        payloadSubTree:add(packet1_t_dwelltime, buffer(pos,2)):append_text(getDwellTimeText(dwellTime)); pos = pos + 2
        
        local vtas = buffer(pos,2):uint()
        payloadSubTree:add(packet1_v_tas, buffer(pos,2)):append_text(getVTasText(vtas)); pos = pos + 2
        
        local d_nextAdvice = buffer(pos,4):uint()
        payloadSubTree:add(packet1_d_nextadvice, buffer(pos,4)):append_text(getDNextAdviceText(d_nextAdvice)); pos = pos + 4
        
        local nextStop = buffer(pos,4):uint();
        payloadSubTree:add(packet1_t_next_stp_arrival_time, buffer(pos,4)):append_text(getNextStopText(nextStop)); pos = pos + 4
        
        local textLength = buffer(pos,1):uint(); pos = pos + 1
        payloadSubTree:add(packet1_l_text_stp, buffer(pos,1))
        if textLength > 0 then
            payloadSubTree:add(packet1_x_text_stp, buffer(pos,textLength)); pos = pos + textLength
        end
        
        local n_stpdistances = buffer(pos, 1):uint(); pos = pos + 1
        payloadSubTree:add(packet1_n_stpdistance_iter, n_stpdistances)
        if n_stpdistances > 0 then
            for i = 1, n_stpdistances, 1 -- this is not c so it starts from 1 and it always contains the last value (in this case n_stpdistances) as well
            do
                distanceInCM = buffer(pos, 4):uint()
                payloadSubTree:add(packet1_d_stpdistance, buffer(pos, 4)):append_text(" Point" .. i .. getDistance(distanceInCM)); pos = pos + 4
            end
        end
    elseif packetNumber == 7 then
        payloadSubTree:add(packet7_n_atoengage_selection, buffer(7,1))
        payloadSubTree:add(packet7_skipstpreq_selection, buffer(8,1))
        payloadSubTree:add(packet7_skipstprev_selection, buffer(9,1))
    else
        payloadSubTree:add(subtree, buffer(), "Unknown data: " .. buffer(7, dataLength))
    end
end

local udp_table = DissectorTable.get("udp.port")
udp_table:add(7920, proto_ato)
udp_table:add(7921, proto_ato)
udp_table:add(7922, proto_ato)
udp_table:add(7923, proto_ato)
