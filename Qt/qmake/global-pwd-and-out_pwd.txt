to have one (or two) variables as globals where the PWD and the shadowed(PWD) 
does not change. Create a .qmake.conf in the root of the project with those
varialbes assingned and then those are globaly the same, even with multiple
projects defined

Example:
.qmake.conf
----
DLD_ROOT=$$PWD
DLD_OUT=$$shadowed($$PWD)
----

Detailed explenation: https://wiki.qt.io/QMake-top-level-srcdir-and-builddir
