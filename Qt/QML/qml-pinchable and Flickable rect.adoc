= qml pinchable and flickable rect

*HORROR*

. Nearly working, scroll jumps a little after pinch, after the returnToBounds() is called

----
import QtQuick 2.6

Rectangle {
    id: root

    property int contentTotalWidth: 4000
    property int contentTotalHeight: 4000

    Flickable {
        id: flick
        anchors.fill: parent
        anchors.margins: 10
        contentWidth: contentTotalWidth
        contentHeight: contentTotalHeight
        clip: true

        PinchArea {
            width: Math.max(flick.contentWidth, flick.width)
            height: Math.max(flick.contentHeight, flick.height)
            pinch.minimumScale: 0.1
            pinch.maximumScale: 2.0

            property real initialWidth
            property real initialHeight

            onPinchStarted: {
                initialWidth = flick.contentWidth
                initialHeight = flick.contentHeight
                flick.interactive = false;
            }

            onPinchUpdated: {
                flick.resizeContent(initialWidth * pinch.scale, initialHeight * pinch.scale, pinch.center)
                mapScale.origin.x = 0
                mapScale.origin.y = 0
                mapScale.xScale = pinch.scale
                mapScale.yScale = pinch.scale
            }

            onPinchFinished: {
                flick.interactive = true;
                flick.returnToBounds()
            }

            Rectangle {
                id: map
                anchors.fill: parent

                transform: Scale {
                    id: mapScale
                    origin.x: 0
                    origin.y: 0
                    xScale: 1
                    yScale: 1
                }

                Repeater {
                    model: modelA
                    delegate: Delegate1 {
                        info: infoRole
                        usedModel: modelA
                    }
                }
                Repeater {
                    model: modelB
                    delegate: Delegate1 {
                        info: infoRole
                        usedModel: modelB
                    }
                }
            }
        }
    }
}

----
