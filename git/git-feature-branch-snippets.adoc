= Git Featurebranch snippets

.Create branch
----
git checkout -b <local-branchname>
----

.Push branch
----
git checkout <local-branchname>
git push --set-upstream origin <remote-branchname>
----

for less confusion remote-branchname should be the same as local-branchname
