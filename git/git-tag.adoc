# Tag the current branch position

. push your current work `git push`
. create the tag `git tag <branch>/<version>`

.Push all tags (should be avoided, since local existing but remote deleted ones will be repushed again)
. push the tags `git push --tags`

.Push created tag
. `git push origin <tag_name>`
