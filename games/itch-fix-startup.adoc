= if the startup of itch fails without an error

. go to `.itch/app.../` and run `./itch --help` this gives some more output
. if the last error line is something like:
+
----
[3911:0225/210556.486125:FATAL:gpu_data_manager_impl_private.cc(445)] GPU process isn't usable. Goodbye.
zsh: trace trap (core dumped)  ./itch --help
----
. the start itch with the following flag `--in-process-gpu`
