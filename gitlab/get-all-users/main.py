#!/usr/bin/env python
import gitlab
import yaml
import csv
import argparse
import sys

parser = argparse.ArgumentParser(prog='get-all-users', description='get user information from the gitlab server')
parser.add_argument('-d', '--debug', action='store_true', help='Use this flag for debugging, then less users are fetched and the first user is printed then the program aborts')

args = parser.parse_args()

isDebug = args.debug

def getConfig():
    with open(".gitlab-access-token.yml", "r") as f:
        try:
            data = yaml.safe_load(f)
        except yaml.YAMLError as exc:
            print(exc)
    return data["gitlab"]

config = getConfig()
token = config["accessToken"]

if len(config["gitlabUrl"]) > 0:
    gl = gitlab.Gitlab(url=config["gitlabUrl"], private_token=token)
else:
    gl = gitlab.Gitlab(private_token=token)


users = []
if isDebug:
    users = gl.users.list(state='active')
else:
    users = gl.users.list(state='active', get_all=True)

userData = []
for u in users:
    if isDebug:
        print(u)
        break

    try:
        createdBy = None
        createdByName = None
        userProvidedVia = "gitlab-intern"

        if u.created_by is not None:
            createdBy = u.created_by["username"]
            createdByName = u.created_by["name"]
        if len(u.identities) > 0:
            userProvidedVia = u.identities[0]["provider"]

        user = { "id": u.id, "name": u.name, "email": u.email, "state": u.state, "bot": u.bot, "userProvidedVia": userProvidedVia, "createdByUsername": createdBy, "createdByName": createdByName, "lastActivity": u.last_activity_on}
        userData.append(user)
    except (AttributeError, IndexError) as error:
        print(error)
        print("Error accessing user propperties. User: ")
        print(u)
        sys.exit(1)

csvFile='/tmp/users.csv'

if len(userData) > 0:
    with open(csvFile, 'w') as f:
        w = csv.DictWriter(f, userData[0].keys())
        w.writeheader()
        for u in userData:
            w.writerow(u)

print(f"Wrote users to: {csvFile}")