#!/usr/bin/env python
import gitlab
import yaml
import csv
import argparse
import sys

parser = argparse.ArgumentParser(prog='get-all-users', description='get user information from the gitlab server')
parser.add_argument('-d', '--debug', action='store_true', help='Use this flag for debugging, then less users are fetched and the first user is printed then the program aborts')

args = parser.parse_args()

isDebug = args.debug

def getConfig():
    with open(".gitlab-access-token.yml", "r") as f:
        try:
            data = yaml.safe_load(f)
        except yaml.YAMLError as exc:
            print(exc)
    return data["gitlab"]

config = getConfig()
token = config["accessToken"]

if len(config["gitlabUrl"]) > 0:
    gl = gitlab.Gitlab(url=config["gitlabUrl"], private_token=token)
else:
    gl = gitlab.Gitlab(private_token=token)

projects = []
groups = []
if isDebug:
    print("getting projects...")
    projects = gl.projects.list()
    print("getting groups...")
    groups = gl.groups.list()
else:
    print("getting projects...")
    projects = gl.projects.list(get_all=True)
    print("getting groups...")
    groups = gl.groups.list(get_all=True)

print("Projects:")
found = False
for project in projects:
    if project.visibility.lower() == "public":
        print(f"  {project.name}")
        found = True
    else:
        print(f"  {project.name}: {project.visibility}")
if not found:
    print("  No project with visibility public")

print("Groups:")
found = False
for group in groups:
    if group.visibility.lower() == "public":
        print(f"  {group.name}")
        found = True
    else:
        print(f"  {group.name}: {group.visibility}")
if not found:
    print("  No group with visibility public")
