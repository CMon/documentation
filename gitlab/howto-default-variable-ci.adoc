= how to set a default variable inside the gitlab ci

for example to have a version set inside an asciidoctor variable:

----
build pdf:
  stage: deploy
  before_script:
    - apk add asciidoctor
    - gem install asciidoctor-pdf
  variables:
    GIT_VERSION: $CI_COMMIT_TAG
  script:
    - mkdir -p public
    - &buildDoc asciidoctor --require asciidoctor-pdf
                            --backend pdf
                            --attribute GIT_VERSION=${GIT_VERSION}
                            --attribute pdf-themesdir=_resources/themes
                            --attribute pdf-theme=pdf
                            --section-numbers
                            gap-analysis-redbox2k/00_main.adoc
                            --out-file public/RedBox_62443-4-2_CENELEC_50701.pdf
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_COMMIT_TAG == null
      variables:
        GIT_VERSION: $CI_COMMIT_SHORT_SHA
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event' || $CI_COMMIT_TAG != null
----

in that case dont forget the workflow:
----
workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_REF_PROTECTED == "true"
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
----
