#!/bin/bash

usage()
{
	echo "Usage: $0 <file:old-device-list> <file:new-device-list>"
	echo ""
	echo "To obtain those lists use the following commands, once before the scan (old-device-list) and once after the scan(s) (new-device-list):"
	echo "For lights:   curl -s --insecure -H \"Content-Type: application/json\" -X GET https://<ip>:8443/rest/devices/lights > old-lights.json"
	echo "For sensors:  curl -s --insecure -H \"Content-Type: application/json\" -X GET https://<ip>:8443/rest/devices/controls/sensors > old-sensors.json"
	echo "For switches: curl -s --insecure -H \"Content-Type: application/json\" -X GET https://<ip>:8443/rest/devices/controls/switches > old-switches.json"
	echo -e "\e[31mDONT FORGET TO CHANGE THE FILENAMES OTHERWISE YOU LOOSE THE OLD STATE\e[39m"
}

if [ "$1" == "" ]; then
	usage
	exit 1
fi
if [ "$2" == "" ]; then
	usage
	exit 1
fi

OLD_DEVICE_LIST=$1
NEW_DEVICE_LIST=$2

# order old and new ids
cat ${OLD_DEVICE_LIST} | jq -r '.devices[].id' | sort > /tmp/old.out
cat ${NEW_DEVICE_LIST} | jq -r '.devices[].id' | sort > /tmp/new.out

# compare old and new
DIFF_DEVICE_IDS=`comm -3 /tmp/old.out /tmp/new.out`

# print all different device IDs
echo "new Ids:"
for dev in ${DIFF_DEVICE_IDS}
do
	echo ${dev}
done
echo ""

# get all complete device elements for each deviceID
echo "new Elements:"
for dev in ${DIFF_DEVICE_IDS}
do
	clean=`echo ${dev} | tr -d '[:space:]'`
	cat ${NEW_DEVICE_LIST} | jq -r ".devices[] | select(.id == \"${dev}\")"
done

