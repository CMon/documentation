It should have been fixed with: https://gitlab.freedesktop.org/pipewire/pipewire/-/issues/640

But if it still does not work then do the stuff from here: ￼ "Rohan Joseph @rohan
:

. get the Server String:
+
----
$ pactl info
Server String: /run/user/1000/pulse/native 
----
.. This will be part of the `pactl` call
. get the "card":
+
----
$ pactl list cards short
40	alsa_card.pci-0000_00_1f.3	alsa
62	bluez_card.98_09_CF_BE_XX_XX	module-bluez5-device.c
----
.. This will be again part of the `pactl` call
. unconnect the headset, then run `udevadm monitor --environment` then reconnect the headset, this should result in an output ala:
+
----
UDEV  [7109.184905] add      /devices/virtual/input/input31/event16 (input)
ACTION=add
DEVPATH=/devices/virtual/input/input31/event16
SUBSYSTEM=input
DEVNAME=/dev/input/event16
SEQNUM=4131
USEC_INITIALIZED=7109172080
ID_INPUT=1
ID_INPUT_KEY=1
ID_BUS=bluetooth
LIBINPUT_DEVICE_GROUP=5/a/ffff:04:d3:b0:c3:XX:XX
MAJOR=13
MINOR=80
TAGS=:power-switch:
CURRENT_TAGS=:power-switch:
----
.. Here remember the mac address in `LIBINPUT_DEVICE_GROUP` this will be the => `ATTRS{phys}` part
. create a udev rule to "fix" the profile A2DP-LDAC does not support input, but has good quality the HSP/HFP:
+
----
$ cat /etc/udev/rules.d/10-bluetooth-headset.rules
ACTION=="add", SUBSYSTEM=="input", ATTRS{phys}=="04:d3:b0:c3:xx:xx", RUN+="/usr/bin/pactl -s /run/user/1000/pulse/native set-card-profile bluez_card.98_09_CF_BE_XX_XX headset-head-unit"
----

