. clone `https://github.com/brianlow/qmk_firmware`
. checkout choc2: `git checkout choc2`
. cd qmk_firmware
. check if `qmk_cli` is used inside: `./util/docker_build.sh` to:
+
----
 77 "$RUNTIME" run --rm -it $usb_args \
 78     $uid_arg \
 79     -w /qmk_firmware \
 80     -v "$dir":/qmk_firmware \
 81     -e ALT_GET_KEYBOARDS=true \
 82     -e SKIP_GIT="$SKIP_GIT" \
 83     -e MAKEFLAGS="$MAKEFLAGS" \
 84     qmkfm/qmk_cli \ <1>
 85     make "$keyboard${keymap:+:$keymap}${target:+:$target}"
----
<1> original image is qmkfm/base_container, but we need qmk_cli
. start flashing with:
----
$ ./util/docker_build.sh
keyboard=sofle_choc
keymap=via
target=flash
----
. follow instructions
.. to reset double press reset button
