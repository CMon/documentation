#!/usr/bin/env python
from pathlib import Path

import pprint
pp = pprint.PrettyPrinter(indent=1)


def removeSubFoldersFromBaseFolders(allAdocs, allBaseFolders):
    """
     removes all subfolders of folders that contain a ".isBaseFolder" file
     since those folders already have a "index"
    """
    retAdoc = []
    for adoc in allAdocs:
        skip = False
        for baseFolder in allBaseFolders:
            if str(adoc.parents[0]).startswith(str(baseFolder.parents[0])):
                directFile = adoc.parents[0] == baseFolder.parents[0]
                if not directFile:
                    skip = True

        if not skip:
            retAdoc.append(adoc)
    return retAdoc

def __createArray(adocs):
    arr = []
    for adoc in adocs:
        arr.append(str(adoc))
    arr.sort()
    return arr

def beautifyChapterName(name):
    beautified = name.replace("-", " ")
    beautified = beautified.replace("_", " ")
    nicer = ""
    # check camel case
    first = True
    for c in beautified:
        if c.isupper():
            nicer = nicer + " "
        if first:
            c = c.upper()
            first = False
        nicer = nicer + c
    return nicer.strip()

filesHandled = []
def createIndexAdoc(lines, arr, chapter):
    for entry in arr:
        if not entry.startswith(chapter):
            continue
        if entry in filesHandled:
            continue
        depth = chapter.count("/") + 2 # +1, because of no leading / another +1 because its inside the global "My documentation"
        indentHeader = "=" * depth

        chapterRemovedRest = entry[len(chapter):]
        if chapterRemovedRest.count("/") > 0:
            parts = chapterRemovedRest.split("/")
            lines.append("")
            lines.append(f"{indentHeader} {beautifyChapterName(parts[0])}")
            createIndexAdoc(lines, arr, f"{chapter}{parts[0]}/")
        else:
            lines.append(f"include::{entry}[leveloffset=+{depth-1}]")
            filesHandled.append(entry)

# pathlib glob all adocs files:
allAdocs = list(Path().glob("**/*.adoc"))
allBaseFolderFiles = list(Path().glob("**/.isBaseFolder"))
adocs = removeSubFoldersFromBaseFolders(allAdocs, allBaseFolderFiles)

lines = []
lines.append("= My Documentation")
lines.append(":icons: font")
lines.append(":source-highlighter: pygments")
lines.append(":toc:")
lines.append(":toclevels: 3")

createIndexAdoc(lines, __createArray(adocs), "")
# pp.pprint(lines)

for line in lines:
	print(line)
