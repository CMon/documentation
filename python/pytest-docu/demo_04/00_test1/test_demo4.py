import subprocess
import os

from smallBenchmarkPlugin.fixtures import add_benchmark_result

def test_cpuinfo():
    p = subprocess.run(["cat", "/proc/cpuinfo"], capture_output=True)
    assert "bogomips" in str(p.stdout)
    add_benchmark_result("bogomips", {"quite a lot"})
