import pytest

def pytest_configure():
    pytest._benchmark_results = {}

def pytest_unconfigure(config):
    print("Benchmark Summaries:")
    print(pytest._benchmark_results)

