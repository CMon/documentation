import subprocess

def test_cpuinfo():
    p = subprocess.run(["cat", "/proc/cpuinfo"], capture_output=True)
    assert "bogomips" in str(p.stdout)
