import pytest
import datetime

def __printAndStore(file: str, message: str):
    if file:
        with open(file, "a") as f:
            f.write(f"{message}\n")
    print(message)

@pytest.hookimpl(trylast=True)
def pytest_configure(config):
    config._cmon_store_benchmark_in_file = config.option.cmon_benchmark_log_file
    pytest._cmon_benchmark_results = {}

@pytest.hookimpl(trylast=True)
def pytest_unconfigure(config):
    __printAndStore(config._cmon_store_benchmark_in_file, f"Benchmark Summaries from {datetime.datetime.now().isoformat()}:")
    br = pytest._cmon_benchmark_results
    for resK, resV in br.items():
        __printAndStore(config._cmon_store_benchmark_in_file, f"  {resK:>20}: {resV}")
    if config._cmon_store_benchmark_in_file:
        print(config._cmon_store_benchmark_in_file, f"Benchmark results updated: {config._cmon_store_benchmark_in_file}")
