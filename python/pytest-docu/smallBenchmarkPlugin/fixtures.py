import pytest

def pytest_addoption(parser):
    group = parser.getgroup('cmon')
    group.addoption('--benchmark-log-file', action='store', dest='cmon_benchmark_log_file', help='log file to store the benchmarks in, if none is given it will be logged to stdout only')

def add_benchmark_result(key: str, results: dict):
    pytest._cmon_benchmark_results[key] = results
