import subprocess
import os

def __input_data():
    return [
            {"filename": "/tmp/docu_1"},
            {"filename": "/tmp/docu_2"},
            {"filename": "/tmp/docu_3"},
        ]

def test_files(subtests):
    for t in __input_data():
        with subtests.test(t=t):
            fn = t["filename"]
            p = subprocess.run(["touch", fn])
            assert p.returncode == 0
            p = subprocess.run(["ls", "/tmp/"], capture_output=True)
            assert p.returncode == 0
            assert os.path.basename(fn) in str(p.stdout)
    
