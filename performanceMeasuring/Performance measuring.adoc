:toc:
:icons: font

= Finding Performance Bottlenecks


== Preperation
.Windows
. Make yourself comfortable with perfmon, to record the performance footprint of only the Application that you want to measure

.Linux
. Make yourself comfortable with grep, ps, top, bash scripts

== Define the Key Performance Idicators (KPIs)
Talk to the customer to figure out what he wants. Common KPIs are CPU Peak, Memory usage, CPU Over Time and Time until something is finished. 


== Define the possible problems that affect the KPIs most
Either you define the possible problems that affect the KPIs by asking the developers or by making blind performance tests.

.Ask Developers
. Ask the developers what factors might be reasons for a performance problem for the given feature

.Analyzing
. Create an environment where you know problems occure 
. make a performance test
. store all corresponding logs
. Figure out which log lines appear to often or what has been done when the time between log lines is larger than expected

Each or the problem components gets an assigned factor (see <<ExampleFactoredProblems, Example>>).

The performance of the application under test is then either a sum, a product or a combination of sum and product of the above components.

[[CreateTestCases]]
== Define Test Cases
After you made a list of KPIs and a list of problems that affect the KPIs most, the test cases should be build on top of that knowledge. 

A test for each Problem has to be made where all remaining Problems stay the same. Each of the problem tests should always have at least 3 tests with different values of the selected problem to figure out if the problem is having a constant, higher or lower growth. During those tests the KPIs have to be monitored in an automatic way. All server, client and KPI logs should be stored for each of the sub test runs, and should only contain the data from the tests. Those logs help the analysts and developers to find coincidences between for example a CPU Peek and what it might cause in the client and/or server. Additionaly the scripts that were used to create those tests should be provided, so developers can recreate those tests during fixing and verify that their fixes realy work.


== Perform tests
After the tests are defined the tests should be run, it is important that the sourrounding environment stays constant. If you are testing a distributed system and your variable under test is not the network then you should move the servers and clients as close together as possible, to not affect the results because of variable or to low network throughput.


== Analyze the data

The test should have produced what variables influence the KPIs in which way. Those should be written down in tables and graphs. preferably the graphs are an overlay of the affected KPI(s) and the variable that was changed, so coincidences could be made visible. 

If the test did not show any performance problems, then either the parameters of the test where wrongly selected or the performance problems occour only on combinations of parameters. When you are sure that the problems are the only ones then you have to go back to <<CreateTestCases, Create Test Cases>> and create test cases where one additional parameter to the already changed one in each test is changed.


== Defining next steps

Filter out the problems that have the biggest impact on the Performance and create UserStories for the developers to investigate and reduce the impact on the KPIs.



== Appendix

=== Example CryptoTec Zone Workroom
This is a stripped down example of a real test we made in the past.

.KIPs defined by customer
. CPU not higher than 50%
. Startup time not longer than 10 minutes

.Problems defined by developers
. Number of files
. Number of changes in one file
. Total Number of changes
. Number of workrooms
. Number of predecessors

[[ExampleFactoredProblems]]
.Each of the problems gets a factor:
. a * <files> 
. b * <changes in one file> 
. c * <changes in all files>
. d * <workrooms>
. e * <predecessors of one file>
. f = <the rest>

.Create Test Cases:
. Number of Files test
[options="header"]
|===
| a    | b | c | d | e | Average CPU(a) | Total TIME(a)
|  100 | 1 | 1 | 1 | 1 |                |
|  200 | 1 | 1 | 1 | 1 |                |
|  500 | 1 | 1 | 1 | 1 |                |
| 1000 | 1 | 1 | 1 | 1 |                |
| 5000 | 1 | 1 | 1 | 1 |                |
|===
[start=2]
. ...

.Analysis
. Filled out tables for the KPI values
. Graphs that show both KPIs and the the changes of the given problem. For example the change of a on the X-Axis, the CPU(a) on the left Y-Axis, the TIME(a) on the right Y-Axis.

.Defining next steps
. Create userstories for the most problematic problems

=== Scripts that can be used as a skeleton for tests

==== Linux

.Create files in a workroom
....
#!/bin/bash

declare -r NUMBER_OF_CHANGES=$1
declare -r WORKROOM_DIR=$2

declare -r NUMBER_OF_FILES=1

function usage() {
    echo "Usage:"
    echo $0" <number-of-file-changes> <workroom-directory>"
}

if [ -z "${NUMBER_OF_CHANGES}" ]
then
    usage
    exit 1
fi

if [ -z "${WORKROOM_DIR}" ]
then
    usage
    exit 1
fi

echo "create files"
dd if=/dev/urandom of=/tmp/foo.dat bs=1024 count=100
for i in `seq 1 ${NUMBER_OF_FILES}`
do
    cat /tmp/foo.dat > "${WORKROOM_DIR}/test$i.dat"
    sleep 1
done

echo "change files"
for f in `seq 1 ${NUMBER_OF_FILES}`
do
    for i in `seq 1 ${NUMBER_OF_CHANGES}`
    do
        echo $i
        dd if=/dev/urandom of=/tmp/foo.dat bs=1024 count=100
        cat /tmp/foo.dat > "${WORKROOM_DIR}/test$f.dat"
        sleep 1
    done
done
....


.Example script to capture cpu usage on a running process on linux
....
#!/bin/bash

declare -r PROGRAM_NAME=$1

function usage() {
    echo "Usage:"
    echo $0" <name-of-program-as-visible-in-top>"
}

if [ -z "${PROGRAM_NAME}" ]
then
    usage
    exit 1
fi

top -b -d 1 | awk "/${PROGRAM_NAME}/ {print strftime(\"%F %T\") \": \"\$0}" <1>
....
<1> See the standard output of top for a description of the single columns after the timestamp.

.Possible errors of top:
. 'dumb': unknown terminal type.
.. Solution:

....
cd /usr/share/terminfo
mkdir d
cp v/vt100 d/dumb
....


== Glossary

|===
| predecessors | A file wich was deleted and a new file that was created with the same name. The deleted then is the predecessor.
| KPI          | Key Performance Indicator
|===
