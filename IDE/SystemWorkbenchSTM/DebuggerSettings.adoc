= Debugging not working

After an installation the paths of the `gdb` (and `openocd` are broken) because they expect a variable that is not set.

The error is (in Details because there you can copy paste):
----
Error with command: /arm-none-eabi-gdb --version
Cannot run program "/arm-none-eabi-gdb": Unknown reason
----
Which shows that it expects the `gdb` in `/`

== Fix

=== Paths
. Open `run->Debug Configurations...`
. now click on the project
. now click on `Debugger`
. And remove the `${...}` from before the `gdb` and the `openocd`
.. If the error openocd not found happens change the `openocd` executable to `"/usr/bin/openocd"` (or wherever it is located, important is the full path AND the `"`
. Apply
. Close

=== Arguments

. since the `openocd` is installed in the system, but the workbench expects to use its own `openocd` targets and interfaces, one has to fix the searchpath for `openocd`:
. add `-s /usr/lib/sw4stm32/plugins/fr.ac6.mcu.debug_2.1.4.201801121207/resources/openocd/st_scripts` to the `openocd options` within the `Debug Configuration` Menu (see fix for paths where to find it)

=== ST-Link V2

. If the `Debug Configurations...` menu does not give you the choice to switch to ST-Link V2, you have to copy the generated file and change the include to st-link-v2 and then specify to use a custom script inside the `Debug Configurations...` menu, the copied and changed one.
