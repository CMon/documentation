= Setup basic Yocto for raspberrypi3

== Open TODOs for this tutorial

. Add qt5.10
.. Add only needed modules
.. do not include X11
. Add custom receipt/layer?
. Add Mender.io

== Create basic directories and checkout needed layers

=== Create directories and fix path
----
mkdir ${HOME}/YOCTO
cd ${HOME}/YOCTO
mkdir bins
cd bins
ln -s /usr/bin/python2 python
ln -s /usr/bin/python2-config python-config
cd ..
----

=== Checkout Sources
----
git clone http://git.yoctoproject.org/git/poky
git clone git://git.yoctoproject.org/meta-raspberrypi
git clone git://git.openembedded.org/meta-openembedded   # contains the meta-python needed for meta-raspberrypi
git clone git://code.qt.io/yocto/meta-qt5.git
git clone git@bitbucket.org:CMon/ions.git ions-src
ln -s ions-src/supplementary/yocto/meta-ions meta-ions
----

=== Checkout correct branches
----
cd poky; git checkout rocko; cd ..
cd meta-raspberrypi; git checkout rocko; cd ..
cd meta-openembedded; git checkout rocko; cd ..
cd meta-qt5; git checkout 5.10; cd ..
----

=== Create startup scrip for later uses

. Create run.sh with following content:
+
----
#!/bin/bash
export PATH=${HOME}/YOCTO/bins:${PATH}
source $HOME/YOCTO/poky/oe-init-build-env rpi3_build
----
. run:
+
----
$ source run.sh
----

ATTENTION: from now on use `source ./run.sh` to get other terminals to work with the set variables (you need to change to this dir beforehand).

== Change configurations to work with raspberrypi 3 and ions

Edit the following files in rpi3_build/conf

=== bblayers.conf

to `BBLAYERS ?=` in `conf/bblayers.conf` add the following but replace `<home>`:

----
  <home>/YOCTO/meta-openembedded/meta-oe \
  <home>/YOCTO/meta-openembedded/meta-multimedia \
  <home>/YOCTO/meta-openembedded/meta-networking \
  <home>/YOCTO/meta-openembedded/meta-python \
  <home>/YOCTO/meta-raspberrypi \
  <home>/YOCTO/meta-qt5 \
----

=== local.conf

* edit `conf/local.conf`:
* change `MACHINE` to: `MACHINE ?= "raspberrypi3"`
* change `PACKAGE_CLASSES` to: `PACKAGE_CLASSES ?= "package_ipk"`

==== Configure Qt5

TODO: have a look into qtbase.inc for configs add them in the setting: `PACKAGECONFIG_append_pn-qt5 = ""`

edit `conf/local.conf` add the following config:

----
IMAGE_INSTALL_append_pn-rpi-basic-image = "packagegroup-qt5-toolchain-target"
----


== build the image

change path to `${HOME}/YOCTO/rpi3_build` and run:
----
$ bitbake ions
----

== clean the ions image build

change path to `${HOME}/YOCTO/rpi3_build` and run:
----
$ bitbake -c clean ions
----
