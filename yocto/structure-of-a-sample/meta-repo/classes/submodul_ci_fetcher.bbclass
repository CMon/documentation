#
# this fetcher has to be inherited, and the src_uri should be git:// instead of gitsm://
# gitsm can not be used since its not possible to define protocol and user for submodules only for the main repo thats possible
python do_fetch_from_http_ci() {
    import subprocess
    import os

    git_dir = f"{d.getVar('WORKDIR', True)}/git"
    proto = d.getVar("GIT_PROTOCOL", True)

    env=None
    if proto == "https":
        # check for config, we can not set it from within here, since we do not have access to CI_JOB_TOKEN from within bitbake
        out, err = bb.process.run("/usr/bin/git config --global --list", log=None, shell=True, stderr=subprocess.PIPE, cwd=None, stdout=subprocess.PIPE)
        if err:
            bb.error(f"Error during getting of configuration: {err}")
        lines = out.split('\n')
        found = False
        for line in lines:
            if line.startswith("url.https://gitlab-ci-token:"):
                found = True
        if not found:
            bb.error('You need to adjust your global .gitconfig with something similar to `git config --global url."https://gitlab-ci-token:${CI_JOB_TOKEN}@git.eitintern.de/".insteadOf "git@git.eitintern.de:"`')
    elif proto == "ssh": # should be ssh
        env={"SSH_AUTH_SOCK" : d.getVar('SSH_AUTH_SOCK')}
    else:
        bb.error(f"{proto} not yet supported within this class")

    bb.process.run("/usr/bin/git submodule init", log=None, shell=True, stderr=subprocess.PIPE, cwd=git_dir, env=env)
    bb.process.run("/usr/bin/git submodule update --recursive", log=None, shell=True, stderr=subprocess.PIPE, cwd=git_dir, env=env)
}

addtask do_fetch_from_http_ci after do_unpack before do_patch
