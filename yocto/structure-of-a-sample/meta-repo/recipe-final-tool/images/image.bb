SUMMARY = "Image for the CMon OS."
LICENSE = "CLOSED"

IMAGE_FEATURES += " splash read-only-rootfs hwcodecs ssh-server-openssh"
DISTRO_FEATURES += " vulkan wayland overlayfs "
DISTRO_FEATURES:remove = " bluetooth pcmcia usbgadget wifi nfs 3g nfc ptest multiarch x11"

IMAGE_FSTYPES:append = " wic.bz2"

MY_GIT_PROTOCOL ?= ""
MY_ADDITIONAL_PROTOCOL_ARGS ?= ""
python() {
    if not d.getVar("MY_ADDITIONAL_PROTOCOL_ARGS", True) or not d.getVar("MY_GIT_PROTOCOL", True):
        bb.error("Please set MY_ADDITIONAL_PROTOCOL_ARGS =\"user=git\" and MY_GIT_PROTOCOL = \"ssh\" or MY_ADDITIONAL_PROTOCOL_ARGS =\"user=gitlab-ci-token:\$\{CI_JOB_TOKEN\}/\" and MY_GIT_PROTOCOL = \"https\" or similar")
}

IMAGE_FSTYPES += " ext4 squashfs"

inherit core-image-minimal

WKS_FILE = "cmon.wks.in"
do_image_wic[depends] += "cmon-boot-image:do_image_complete"

CMON_QT_MODULES = "\
 packagegroup-qt6-essentials \
 qtsvg \
 qtpositioning \
 qtgrpc \
 qtwayland \
 "

PACKAGECONFIG_pn-qtbase:append = " wayland"

CORE_IMAGE_BASE_INSTALL += "\
 rauc \
 rauc-conf \
 ${@bb.utils.contains('DISTRO_FEATURES', 'x11', 'weston-xwayland matchbox-terminal', '', d)} \
 ethtool \
 cage \
 sudo \
 syslog-ng \
 ntp \
 net-snmp \
 openldap \
 iptables \
 iproute2 \
 iproute2-ss \
 iproute2-tc \
 init-ifupdown \
 nginx \
 tpm2-tools \
 docker-moby \
 docker-compose \
 ${CMON_QT_MODULES} \
 "

QB_MEM = "-m 512"

IMAGE_ROOTFS_SIZE ?= "8192"
IMAGE_ROOTFS_EXTRA_SPACE:append = "${@bb.utils.contains("DISTRO_FEATURES", "systemd", " + 4096", "", d)}"

#TODO: to be moved
TOOLCHAIN_HOST_TASK:append = " \
 nativesdk-protobuf-c \
 "

inherit populate_sdk_qt6

OVERRIDES = "${@bb.utils.contains('MACHINE_FEATURES', 'dev', 'dev', '', d)}"

PACKAGECONFIG_pn-generic-gateway:append = " All"
# PACKAGECONFIG_pn-generic-gateway:append = " Mouse App Itgp Keyboard Udp Tcp RedundancyFilter"

IMAGE_FEATURES:append:dev = " debug-tweaks serial-autologin-root tools-debug"

# remove the sysroot of the target from the sdk
SDKIMAGE_FEATURES:remove = " dbg-pkgs"

IMAGE_INSTALL:append:dev = " \
 cpio \
 sysfsutils \
 shadow \
 tcpdump \
 systemd-bootchart \
 dool \
 "

verity_setup:append () {
    echo "ADDITIONAL_OPTIONS=${@bb.utils.contains('MACHINE_FEATURES', 'dev', '--ignore-corruption', '--restart-on-corruption', d)}" >> ${STAGING_VERITY_DIR}/${IMAGE_BASENAME}.$TYPE.verity.env

    # Grow the Verity image by 1 block to make room for verity flag
    # Generally, I would expect the partition holding this fs image to
    # be much larger than this image. In the event it isn't, this is a
    # safeguard.
    truncate -s +${align} ${OUTPUT_HASH}

    # Fill holes in the file so bmaptool will write out zeros to the
    # target media. If this is not done, dm-verity may detect unwritten
    # sections as mismatching.
    fallocate -l $(stat -L -c %s ${OUTPUT_HASH}) ${OUTPUT_HASH}
}
