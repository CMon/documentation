inherit bundle

#LICENSE = "CLOSED"

#RAUC_BUNDLE_FORMAT = "verity"

RAUC_BUNDLE_SLOTS = "root boot"

RAUC_IMAGE_FSTYPE = "tar.bz2"

RAUC_BUNDLE_COMPATIBLE = "cmon"

RAUC_SLOT_boot = "cmon-boot-image"
RAUC_SLOT_boot[file] = "${BOOT_IMAGE_NAME}"
RAUC_SLOT_boot[type] = "image"
RAUC_SLOT_boot[rename] = "boot.img"

RAUC_SLOT_root = "cmon-image"
RAUC_SLOT_root[file] = "${DM_VERITY_IMAGE}-${MACHINE}.rootfs.${DM_VERITY_IMAGE_TYPE}.verity"
RAUC_SLOT_root[type] = "image"
RAUC_SLOT_root[rename] = "rootfs.img"

