# recipe for my-tool
...
PACKAGES = "${PN} ${PN}-debug-tools"
...
then inside the FILES sections:

FILES = ""
FILES:${PN} = " \
    /usr/bin/my-tool \
    /etc/my-tool/my.conf \
"

FILES:${PN}-debug-tools = " \
    /usr/bin/my-tool-cli \
"

do_install() {
    install -m 770 ${B}/bin/my-tool ${D}/usr/bin/

    install -m 770 ${B}/bin/my-tool-cli ${D}/usr/bin/
}

EXTRA_OECMAKE += " -DBUILD_TESTING=OFF"

# see the , that devides the <enabled>,<not enabled>
PACKAGECONFIG:${PN}-debug-tools = "-DWITH_CLI=ON,-DWITH_CLI=OFF"

...

Then inside your image.bb

install the my-tool in your default image, and append my-tool-debug-tools to the debug image
