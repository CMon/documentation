# recipe for my-tool
# PACKAGECONFIG options:
#  - All                Enables all Gateway plugins
#  - App                Enables the App Gateway plugin
#  - Can                Enables the Can Gateway plugin
#  - tools-debug        Builds additional tools that help during development

...

PACKAGECONFIG ??= ""
PACKAGECONFIG[tools-debug] = "-DWITH_TOOLS=ON,-DWITH_TOOLS=OFF"

PLUGINS_ARG:append = "${@bb.utils.contains('PACKAGECONFIG', 'All', 'ALL;', '', d)}"
PLUGINS_ARG:append = "${@bb.utils.contains('PACKAGECONFIG', 'App', 'App;', '', d)}"
PLUGINS_ARG:append = "${@bb.utils.contains('PACKAGECONFIG', 'Can', 'Can;', '', d)}"

...

DEPENDS += " \
 ${@bb.utils.contains('PACKAGECONFIG', 'All', 'qtscxml', '', d)} \
 ${@bb.utils.contains('PACKAGECONFIG', 'App', 'qtscxml', '', d)} \
"

RDEPENDS:${PN} += " \
 ${@bb.utils.contains('PACKAGECONFIG', 'All', 'qtscxml', '', d)} \
 ${@bb.utils.contains('PACKAGECONFIG', 'App', 'qtscxml', '', d)} \
"
...
EXTRA_OECMAKE += " -DBUILD_PLUGINS=\"${PLUGINS_ARG}\""


then inside your image.bb, add something like this:

PACKAGECONFIG_pn-my-tool:append = " App"
PACKAGECONFIG_pn-my-tool:append:dev = " tools-debug" # :dev in this case is the development machine, replace with whatever you use
