#!/bin/bash

set -e

SO_PIN=1234
USER_PIN=4321
TOKEN_NAME="mySoftHSM"
EXAMPLE_KEY_LABEL="SoftHSM Testkey"

export SOFTHSM2_MODULE=/usr/lib/pkcs11/libsofthsm2.so

pkcs11-tool --module ${SOFTHSM2_MODULE} --init-token --label ${TOKEN_NAME} --so-pin ${SO_PIN} >/dev/null 2>&1
pkcs11-tool --module ${SOFTHSM2_MODULE} --login --so-pin ${SO_PIN} --new-pin ${USER_PIN} --init-pin >/dev/null 2>&1

pkcs11-tool --module ${SOFTHSM2_MODULE} --login --pin ${USER_PIN} --keypairgen --key-type rsa:4096 --id 10 --label "${EXAMPLE_KEY_LABEL}" >/dev/null 2>&1
OPENSSL_CONF=./hsm.conf openssl req -engine pkcs11 -keyform engine -new -key "pkcs11:token=${TOKEN_NAME}" -sha256 -x509 -days 365 -out CA_cert.pem -subj '/C=DE/ST=NRW/CN=TestName' >/dev/null 2>&1
openssl x509 -in CA_cert.pem -out CA_cert.der -outform der >/dev/null 2>&1
pkcs11-tool --module ${SOFTHSM2_MODULE} --login --pin ${USER_PIN} --write-object CA_cert.der --type cert --label "${EXAMPLE_KEY_LABEL}" >/dev/null 2>&1
rm CA_cert.*

echo "Created softhsm:"
echo " SO-PIN:   ${SO_PIN}"
echo " User-PIN: ${USER_PIN}"
echo "Entries:"
pkcs11-tool --module ${SOFTHSM2_MODULE}  --list-object --login --pin ${USER_PIN}
echo
echo
echo
echo "To use it you need following exports:"
echo
echo " export SOFTHSM2_MODULE=${SOFTHSM2_MODULE}"
echo " export RAUC_PKCS11_PIN=${USER_PIN}"
echo " export RAUC_PKCS11_MODULE=\${SOFTHSM2_MODULE}"
echo
echo "Example usage:"
echo " Create Key:"
echo "  pkcs11-tool --module \${SOFTHSM2_MODULE} --login --keypairgen --key-type rsa:4096 --id 10 --label \"${EXAMPLE_KEY_LABEL}\""
echo " Create certificate:"
echo "  OPENSSL_CONF=./hsm.conf openssl req -engine pkcs11 -keyform engine -new -key \"pkcs11:token=${TOKEN_NAME}\" -sha256 -x509 -days 365 -out CA_cert.pem -subj '/C=DE/ST=NRW/CN=TestName'"
echo " Write certificate to »${TOKEN_NAME}«:"
echo "  openssl x509 -in CA_cert.pem -out CA_cert.der -outform der"
echo "  pkcs11-tool --module \${SOFTHSM2_MODULE} --login --write-object CA_cert.der --type cert --label \"${EXAMPLE_KEY_LABEL}\""
echo
echo " Check that the keys/certs are there:"
echo "  pkcs11-tool --module \${SOFTHSM2_MODULE} --list-object --login"
echo " Get the pkcs11 urls: "
echo "  p11tool --list-all"
echo "  p11tool --list-all \"pkcs11:token=${TOKEN_NAME}\""
