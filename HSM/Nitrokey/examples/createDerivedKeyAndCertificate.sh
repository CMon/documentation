#!/bin/bash

EXPIRE_DAYS=3650

ROOT_KEY_LABEL=${1}
BASENAME=${2}

function usage {
    echo "$0 <rootKeyLabel> <basename>"
    echo
    echo "Optionals:"
    echo "  <rootKeyLabel>  the label for the rootkey, needs to be encoded like its listed with the p11tool"
    echo "  <baseName>      the base name for the derived key (for example MyOtherDomain)"
    echo "Possible env variables:"
    echo "  <USER_PIN>      the user pin that should be handed over to the pkcs11-tool, if not set the tools will ask for it"
    echo
    echo "To have a fully non-interactive experiance, one needs to fill the `PIN` in `hsm.conf` and use the env-var described above"
    exit 1
}

if [[ ${ROOT_KEY_LABEL} == -* ]]
then
    usage
fi

PIN_PART=""
if [[ "${USER_PIN}" != "" ]]
then
    PIN_PART="--pin ${USER_PIN}"
fi

# check if root-key exists
echo "* Checking if the root key exists for »${ROOT_KEY_LABEL}«"
p11tool --provider opensc-pkcs11.so --list-all "pkcs11:object=${ROOT_KEY_LABEL};type=public" > /dev/null 2>&1
if [ $? -ne 0 ]
then
    echo -e "** Could not find root key, aborting."
    exit 1
fi

# check if customer-key exists, create it if not
echo "* Checking if the customer key (»${BASENAME}«) already exists"
p11tool --provider opensc-pkcs11.so --list-all "pkcs11:object=${BASENAME};type=public" > /dev/null 2>&1
if [ $? -ne 0 ]
then
    echo -e "** Did not found key for »${BASENAME}« creating it..."
    pkcs11-tool --module opensc-pkcs11.so --login ${PIN_PART} --keypairgen --key-type rsa:4096 --label ${BASENAME}
fi

# check if customer-certificate exists, create it if not
echo "* Checking if the customer certificate (»${BASENAME}«) exists"
p11tool --provider opensc-pkcs11.so --list-all "pkcs11:object=${BASENAME};type=cert"
if [ $? -ne 0 ]
then
    echo -e "** Certificate does not exist creating it"
    CRT_FILE="${BASENAME}.pem"
    CSR_FILE="${BASENAME}.csr"
    DER_FILE="${BASENAME}.der"
    EXT_FILE="${BASENAME}.ext"
    ROOT_CRT_DER="${ROOT_KEY_LABEL}.cert.der"
    ROOT_CRT_PEM="${ROOT_KEY_LABEL}.cert.pem"


    set -e # if one of the following fails abort

    # first create certificate signing request
    echo "** Create Certificate Signing Request"
    OPENSSL_CONF=./Nitrokey-HSM2.conf openssl req -engine pkcs11 -keyform engine -new \
            -key "pkcs11:object=${BASENAME};type=private" -out ${CSR_FILE} -subj "/C=DE/ST=NRW/L=BGL"

    # create extension file
    echo -e "[ v3_leaf_codesign ]\nsubjectKeyIdentifier=hash\nauthorityKeyIdentifier=keyid:always,issuer:always\nbasicConstraints = CA:FALSE\nextendedKeyUsage=critical,codeSigning\n" > ${EXT_FILE}

    # sign certificate with root-ca
    echo "** Sign Certificate with root key"
    # extract root cert from pkcs11
    pkcs11-tool --read-object --label ${ROOT_KEY_LABEL} --type cert --module opensc-pkcs11.so --output-file ${ROOT_CRT_DER}
    openssl x509 -inform DER -in ${ROOT_CRT_DER} -pubkey > ${ROOT_CRT_PEM}

    # sign with root cert, root key
    OPENSSL_CONF=./Nitrokey-HSM2.conf openssl x509 -req -engine pkcs11 -CAkeyform engine -in ${CSR_FILE} \
                                    -CA ${ROOT_CRT_PEM} -CAkey "pkcs11:object=${ROOT_KEY_LABEL};type=private" \
                                    -CAcreateserial -out ${CRT_FILE} -days ${EXPIRE_DAYS} -sha256 -extfile ${EXT_FILE}

    # now store it on the HSM
    echo -e "** Certificate created, storing it on the HSM"
    openssl x509 -in ${CRT_FILE} -out ${DER_FILE} -outform der
    pkcs11-tool --login ${PIN_PART} --write-object ${DER_FILE} --type cert --label ${BASENAME}

    set +e # now we may fail again

    echo -e "** Removing intermediate files"
    rm -f ${CRT_FILE}
    rm -f ${CSR_FILE}
    rm -f ${DER_FILE}
    rm -f ${EXT_FILE}
    rm -f ${ROOT_CRT_DER}
    rm -f ${ROOT_CRT_PEM}
    rm *.srl
fi

echo "DONE"
