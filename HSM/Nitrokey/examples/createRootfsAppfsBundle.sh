#!/bin/bash

set -e

RAUCB_FILE=${1:-"update-2020.raucb"}
APPFS_FILEPATH=${2:-"appfs.tar.gz"}
CERT_LINE=${3}
KEY_LINE=${4}
DESTINATION_RAUC=${5}

usage() {
    echo "Usage:"
    echo "  ${0} <baseRaucb> <appfs> <CERT> <KEY> [<destinationRaucbFile>]"
    echo
    echo "Arguments:"
    echo "  baseRaucb             The path to the .raucb file that contains the root image and no appimage"
    echo "  appfs                 The path to the .tar.gz file that contains the application (excluding the leading /opt)"
    echo "  CERT                  Either the path to the .cert.pem file or a pkcs11 line like: »pkcs11:serial=<serial>;object=<label>« (instead of serial you could use token as well)"
    echo "  KEY                   Either the path to the .key.pem file or a pkcs11 line like: »pkcs11:serial=<serial>;object=<label>« (instead of serial you could use token as well)"
    echo "  destinationRaucbFile  The path to the file that the new image will be stored to"
    echo
    echo "Optional environment variables:"
    echo "  RAUC_BINARY           If not set we try to get »rauc« with \`which rauc\`"
    echo "  UNSQUASHFS_BINARY     If not set we try to get »unsquashfs« with \`which unsquashfs\`"
    echo "  RAUC_PKCS11_PIN       The user pin Rauc utilizes for accessing the <KEY>"
    exit 1
}

error() {
    echo -e "\nERROR: \e[31m$1\e[0m" # red
    usage
}

errorAndExit() {
    echo -e "\nERROR: \e[31m$1\e[0m" # red
    exit 1
}

warn() {
    echo -e "INFO: \e[33m$1\e[0m" # yellow
}

info() {
    echo -e "INFO: \e[32m$1\e[0m" # green
}

if [[ ${RAUCB_FILE} == -* ]]
then
    usage
fi

if [ ! -f "${RAUCB_FILE}" ]; then
    error "* Missing update image: »${RAUCB_FILE}«"
fi

if [ ! -f "${APPFS_FILEPATH}" ]; then
    error "* Missing appfs: »${APPFS_FILEPATH}«"
fi

if [ -z "${CERT_LINE}" ]; then
    error "* Missing »CERT« argument"
fi

if [ -z "${KEY_LINE}" ]; then
    error "* Missing »KEY« argument"
fi

if [ -z "${DESTINATION_RAUC}" ]; then
    dir=$(dirname "${RAUCB_FILE}")
    base=$(basename "${RAUCB_FILE}" .raucb)
    DESTINATION_RAUC="$dir/$base-withApp.raucb"
    info "* Destination rauc is not set setting it to: ${DESTINATION_RAUC}"
fi

if [ -f "${DESTINATION_RAUC}" ]; then
    warn "* ${DESTINATION_RAUC} already exists, removing it."
    rm -f ${DESTINATION_RAUC}
fi

if [ -z "${RAUC_BINARY}" ]; then
    RAUC_BINARY=$(which rauc)
    if [ -z "${RAUC_BINARY}" ]; then
        error "* Could not find »rauc« in your \$PATH, either install it or set the ENV variable »RAUC_BINARY« to the location of rauc"
    fi
fi
info "* Using »rauc« from: ${RAUC_BINARY}"

if [ -z "${UNSQUASHFS_BINARY}" ]; then
    UNSQUASHFS_BINARY=$(which unsquashfs)
    if [ -z "${UNSQUASHFS_BINARY}" ]; then
        error "* Could not find »unsquashfs« in your \$PATH, either install it or set the ENV variable »UNSQUASHFS_BINARY« to the location of rauc"
    fi
fi
info "* Using »unsquashfs« from: ${UNSQUASHFS_BINARY}"

TMP_DIR=$(mktemp -d -t "update-updatePackage-XXXXX")

trap 'cleanup' ERR
cleanup() {
    info "* Cleanup ${TMP_DIR}"
    rm -rf ${TMP_DIR}
}

RAUC_MANIFEST=${TMP_DIR}/manifest.raucm
INSTALL_HOOKS_FILE=${TMP_DIR}/install-hooks.sh
APPFS_FILE=$(basename ${APPFS_FILEPATH})

info "* Unpacking current update package ${RAUCB_FILE} to ${TMP_DIR}"
${UNSQUASHFS_BINARY} -f -d ${TMP_DIR} "${RAUCB_FILE}"

if [ ! -f "${RAUC_MANIFEST}" ]; then
    errorAndExit "** Update does not have the expected format (missing »manifest.raucm«) aborting"
fi

if [ ! -f "${INSTALL_HOOKS_FILE}" ]; then
    errorAndExit "** Update does not have the expected format (missing »install-hooks.sh«) aborting"
fi

info "* Adding application fs ${APPFS_FILE} from ${APPFS_FILEPATH}"
cp ${APPFS_FILEPATH} ${TMP_DIR}/${APPFS_FILE}
echo >> ${RAUC_MANIFEST}
echo -e "[image.appfs]\nfilename=${APPFS_FILE}\nhooks=post-install;\n" >> ${RAUC_MANIFEST}

sed -i 's/KEEP_SETTINGS=0/KEEP_SETTINGS=1/g' ${INSTALL_HOOKS_FILE}

info "* Re-bundle & sign update"
${RAUC_BINARY} \
    --cert ${CERT_LINE} --key ${KEY_LINE} \
    bundle \
    ${TMP_DIR} "${DESTINATION_RAUC}"

cleanup

info "* Placed signed bundle here: ${DESTINATION_RAUC}"
