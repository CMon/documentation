#!/bin/bash

EXPIRE_DAYS=10950

PARENT_KEY_LABEL=${1}
BASENAME=${2}
TOKENNAME=${3:-"myToken"}
PROVIDER=${4:-"libsofthsm2.so"}

function usage {
    echo "$0 <devisionName> <basename> [<tokenName> [<provider-lib>]]"
    echo
    echo "Optionals:"
    echo "  <devisionName>  the label for the devision (for example SoftwareDevelopment), needs to be encoded like its listed with the p11tool"
    echo "  <baseName>      the base name for the customerkey (for example Siemens-GEMMI)"
    echo "Possible env variables:"
    echo "  <USER_PIN>      the user pin that should be handed over to the pkcs11-tool, if not set the tools will ask for it"
    echo
    echo "To have a fully non-interactive experiance, one needs to fill the »PIN« in »Nitrokey-HSM2.conf« and use the env-var described above"
    exit 1
}

if [[ ${PARENT_KEY_LABEL} == -* ]]
then
    usage
fi

if [[ "${PARENT_KEY_LABEL}" == "" ]]
then
    usage
fi

PIN_PART=""
if [[ "${USER_PIN}" != "" ]]
then
    PIN_PART="--pin ${USER_PIN}"
fi

PKCS11_BASEURL_INTERMEDIATE="pkcs11:token=${TOKENNAME};object=${PARENT_KEY_LABEL}"
PKCS11_BASEURL_CUST="pkcs11:token=${TOKENNAME};object=${BASENAME}"

# check if intermediate-key exists
echo "* Checking if the intermediate key exists for »${PARENT_KEY_LABEL}«"
p11tool --provider ${PROVIDER} --list-all "${PKCS11_BASEURL_INTERMEDIATE};type=public" > /dev/null 2>&1
if [ $? -ne 0 ]
then
    echo -e "** Could not find intermediate key, aborting."
    exit 1
fi

# check if customer-key exists, create it if not
echo "* Checking if the customer key (»${BASENAME}«) already exists"
p11tool --provider ${PROVIDER} --list-all "${PKCS11_BASEURL_CUST};type=public" > /dev/null 2>&1
if [ $? -ne 0 ]
then
    echo -e "** Did not found key for »${BASENAME}« creating it..."
    pkcs11-tool --module ${PROVIDER} --login ${PIN_PART} --keypairgen --key-type rsa:4096 --label ${BASENAME}
fi

# check if customer-certificate exists, create it if not
echo "* Checking if the customer certificate (»${BASENAME}«) exists"
p11tool --provider ${PROVIDER} --list-all "${PKCS11_BASEURL_CUST};type=cert"
if [ $? -ne 0 ]
then
    echo -e "** Certificate does not exist creating it"
    CRT_FILE="${BASENAME}.pem"
    CSR_FILE="${BASENAME}.csr"
    DER_FILE="${BASENAME}.der"
    EXT_FILE="${BASENAME}.ext"
    INTERMEDIATE_CRT_DER="${PARENT_KEY_LABEL}.cert.der"
    INTERMEDIATE_CRT_PEM="${PARENT_KEY_LABEL}.cert.pem"


    set -e # if one of the following fails abort

    # first create certificate signing request
    echo "** Create Certificate Signing Request"
    OPENSSL_CONF=./SoftHSM.conf openssl req -engine pkcs11 -keyform engine -new \
            -key "${PKCS11_BASEURL_CUST};type=private" -out ${CSR_FILE} -subj "/C=DE/ST=NRW/L=BGL"

    # sign certificate with intermediate-ca
    echo "** Sign Certificate with intermediate key"
    # extract intermediate cert from pkcs11
    pkcs11-tool --read-object --label ${PARENT_KEY_LABEL} --type cert --module ${PROVIDER} --output-file ${INTERMEDIATE_CRT_DER}
    openssl x509 -inform DER -in ${INTERMEDIATE_CRT_DER} -pubkey > ${INTERMEDIATE_CRT_PEM}

    # sign with intermediate cert, intermediate key
    OPENSSL_CONF=./SoftHSM.conf openssl x509 -req -engine pkcs11 -CAkeyform engine -in ${CSR_FILE} \
                                    -CA ${INTERMEDIATE_CRT_PEM} -CAkey "${PKCS11_BASEURL_INTERMEDIATE};type=private" \
                                    -CAcreateserial -out ${CRT_FILE} -days ${EXPIRE_DAYS} -sha256 -extfile openssl.cnf -extensions v3_leaf_codesign

    # now store it on the HSM
    echo -e "** Certificate created, storing it on the HSM"
    openssl x509 -in ${CRT_FILE} -out ${DER_FILE} -outform der
    pkcs11-tool --module ${PROVIDER} --login ${PIN_PART} --write-object ${DER_FILE} --type cert --label ${BASENAME}

    set +e # now we may fail again

    echo -e "** Removing intermediate files"
    rm -f ${CRT_FILE}
    rm -f ${CSR_FILE}
    rm -f ${DER_FILE}
    rm -f ${EXT_FILE}
    rm -f ${INTERMEDIATE_CRT_DER}
    rm -f ${INTERMEDIATE_CRT_PEM}
    rm *.srl
fi

echo "DONE"
