#!/bin/bash

DEFAULT_BASE="Deuta-Root"
EXPIRE_DAYS=10950

BASENAME=${1:-${DEFAULT_BASE}}
TOKENNAME=${2:-"myToken"}
PROVIDER=${3:-"libsofthsm2.so"}

function usage {
    echo "$0 [<baseName> [<tokenName> [<provider-lib>]]]"
    echo
    echo "Optionals:"
    echo "  <baseName>   the base name for the root key, if kept blank »${DEFAULT_BASE}« will be used"
    echo "Possible env variables:"
    echo "  <USER_PIN>   the user pin that should be handed over to the pkcs11-tool, if not set the tools will ask for it"
    exit 1
}

if [[ ${BASENAME} == -* ]]
then
    usage
fi

PIN_PART=""
if [[ "${USER_PIN}" != "" ]]
then
    PIN_PART="--pin ${USER_PIN}"
fi

PKCS11_BASEURL="pkcs11:token=${TOKENNAME};object=${BASENAME}"
echo "* using pkcs11base url: ${PKCS11_BASEURL}"

# check if key exists
echo "* Checking if a private key already exists for »${BASENAME}«"
p11tool --provider ${PROVIDER} --list-all "${PKCS11_BASEURL};type=public" > /dev/null 2>&1
if [ $? -ne 0 ]
then
    echo -e "** Did not found key for »${BASENAME}« creating it..."
    pkcs11-tool --module ${PROVIDER} --login ${PIN_PART} --keypairgen --key-type rsa:4096 --label ${BASENAME}
fi

# check if cert exists
echo "* Checking if a certificate exists for »${BASENAME}«"
p11tool --provider ${PROVIDER} --list-all "${PKCS11_BASEURL};type=cert"
if [ $? -ne 0 ]
then
    CRT_FILE="${BASENAME}.pem"
    DER_FILE="${BASENAME}.der"
    EXT_FILE="${BASENAME}.ext"

    set -e # if one of the following fails abort

    echo -e "** Certificate does not exist creating it"
    OPENSSL_CONF=./SoftHSM.conf openssl req -engine pkcs11 -keyform engine -new \
            -key "${PKCS11_BASEURL};type=private" -sha256 -config openssl.cnf -extensions v3_ca \
            -x509 -days ${EXPIRE_DAYS} -out ${CRT_FILE} -subj '/C=DE/ST=NRW/L=BGL/O=Deuta/CN=Deuta.de'

    echo -e "** Certificate created, storing it on the HSM"
    openssl x509 -in ${CRT_FILE} -out ${DER_FILE} -outform der
    pkcs11-tool --module ${PROVIDER} --login ${PIN_PART} --write-object ${DER_FILE} --type cert --label ${BASENAME}

    set +e # now we may fail again

    echo -e "** Removing intermediate files"
    rm -f ${CRT_FILE}
    rm -f ${DER_FILE}
fi

echo "DONE"
