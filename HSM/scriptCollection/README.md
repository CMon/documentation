# CertificateScriptCollection

* use `createRootKeyAndCertificate.sh` to create a new root key, this should not be done but is here in case our root key got compromised and we need a new one
* use `createCustomerKeyAndCertificate.sh` to create a certificate for a customer
