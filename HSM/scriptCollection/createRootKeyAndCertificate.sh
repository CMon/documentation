#!/bin/bash

DEFAULT_BASE="Deuta-Root"
EXPIRE_DAYS=10950

BASENAME=${1:-${DEFAULT_BASE}}

function usage {
    echo "$0 [<baseName>]"
    echo
    echo "Optionals:"
    echo "  <baseName>   the base name for the root key, if kept blank »${DEFAULT_BASE}« will be used"
    echo "Possible env variables:"
    echo "  <USER_PIN>   the user pin that should be handed over to the pkcs11-tool, if not set the tools will ask for it"
    exit 1
}

if [[ ${BASENAME} == -* ]]
then
    usage
fi

PIN_PART=""
if [[ "${USER_PIN}" != "" ]]
then
    PIN_PART="--pin ${USER_PIN}"
fi

# check if key exists
echo "* Checking if a private key already exists for »${BASENAME}«"
p11tool --provider opensc-pkcs11.so --list-all "pkcs11:object=${BASENAME};type=public" > /dev/null 2>&1
if [ $? -ne 0 ]
then
    echo -e "** Did not found key for »${BASENAME}« creating it..."
    pkcs11-tool --module opensc-pkcs11.so --login ${PIN_PART} --keypairgen --key-type rsa:4096 --label ${BASENAME}
fi

# check if cert exists
echo "* Checking if a certificate exists for »${BASENAME}«"
p11tool --provider opensc-pkcs11.so --list-all "pkcs11:object=${BASENAME};type=cert"
if [ $? -ne 0 ]
then
    CRT_FILE="${BASENAME}.pem"
    DER_FILE="${BASENAME}.der"

    echo -e "** Certificate does not exist creating it"
    OPENSSL_CONF=./Nitrokey-HSM2.conf openssl req -engine pkcs11 -keyform engine -new \
            -key "pkcs11:object=${BASENAME};type=private" -sha256 -x509 -days ${EXPIRE_DAYS} -out ${CRT_FILE} -subj '/C=DE/ST=NRW/L=BGL/O=Deuta/CN=Deuta.de'

    echo -e "** Certificate created, storing it on the HSM"
    openssl x509 -in ${CRT_FILE} -out ${DER_FILE} -outform der
    pkcs11-tool --login ${PIN_PART} --write-object ${DER_FILE} --type cert --label ${BASENAME}

    echo -e "** Removing intermediate files"
    rm -f ${CRT_FILE}
    rm -f ${DER_FILE}
fi

echo "DONE"
